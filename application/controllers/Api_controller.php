<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'third_party/phpToPDF.php';
class Api_controller extends CI_Controller {
	//require("fcm.php");
	
	public function __construct()
	{
		parent::__construct();
	//	$fcm = new Fcm();
		$this->load->model('Api_model');
		$this->load->model('Model_basic');
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper('imgupload');
		header('Content-Type: application/json');
	}
	public function multiple_image()
	{
		if(!empty($_FILES))
		{
			 $filesCount = count($_FILES);
			for($i=1; $i<=$filesCount;$i++)
			{
			//	echo "in";
			$base_path="uploads/test";
			//mkdir($base_path,0777);
			if(!(file_exists($base_path)))
				{
					//echo "hey";
					mkdir($base_path,0777);
				}
		//	echo $base_path;die;
				//echo $_FILES['profileimg']['name'][$i];
				$_FILES['image'.$i]['name'] = $_FILES['image'.$i]['name'];
				$_FILES['image'.$i]['type'] = $_FILES['image'.$i]['type'];
				$_FILES['image'.$i]['tmp_name'] = $_FILES['image'.$i]['tmp_name'];
				$_FILES['image'.$i]['error'] = $_FILES['image'.$i]['error'];
				$_FILES['image'.$i]['size'] = $_FILES['image'.$i]['size'];
				$config['upload_path'] =$base_path;
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				//echo $_FILES['profileimg']['name'];
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('image'.$i))
				{
					echo "hii";
					
					//echo $res;die;
				}else
				{
					echo "error";
					 echo $this->upload->display_errors();
				}
			}
		}
	}
	public function register_new_seller()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("org_name","email","phone_no","address","city","zip","password");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("org_name","email","phone_no","address","city","zip","password");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$uniqeid=$this->Api_model->check_isEmailUniqe("Seller",$decodeobj['email']);
						if(empty($uniqeid))
						{
								$data1=array('org_name'=>$decodeobj['org_name'],'company_reg_no'=>'','phone_no'=>$decodeobj['phone_no'],'address'=>$decodeobj['address'],'city'=>$decodeobj['city'],'zip'=>$decodeobj['zip']);
								$res=$this->Api_model->insert_query("seller",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to register";
									
								}else
								{
									$login_data=array("user_id"=>$res,"user_flag"=>"Seller","email"=>$decodeobj['email'],"password"=>md5($decodeobj["password"]),"status"=>1,"created_on"=>date('Y-m-d H:i:s'));
									
									$login_id=$this->Api_model->insert_query("login_detail",$login_data);
									$email_data=array("name"=>$decodeobj['org_name'],"flag"=>"Seller","message"=>"Thank you for signing up with Ezyscrap. !!!");
										$message=$this->registration_email($email_data);
										$msg=$message;
									$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Successfully registered','message'=>$msg);
									$this->Model_basic->sendEmail($data);
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Seller Registered Successfully";	
								}
							
						}else
						{
							$senddata['code']=400;
							$senddata['status']="Failed";
							$senddata['description']="This Email id already used";
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function update_new_seller()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("org_name","phone_no","address","city","zip","token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("org_name","phone_no","address","city","zip","token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],'Seller')!='')
						{
						
								$data1=array('org_name'=>$decodeobj['org_name'],'company_reg_no'=>'','phone_no'=>$decodeobj['phone_no'],'address'=>$decodeobj['address'],'city'=>$decodeobj['city'],'zip'=>$decodeobj['zip']);
								$res=$this->Api_model->update_query("seller",$this->authentic_user($decodeobj['token'],'Seller'),$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to register";
									
								}else
								{
									
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Successfully updated profile";	
								}
							
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function register_new_buyer()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=$this->input->post();
			if(!empty($decodeobj))
			{
				$isExistArr=array("name","email","phone_no","address","password","city","zip");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("name","email","phone_no","address","password","city","zip");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$uniqeid=$this->Api_model->check_isEmailUniqe("Buyer",$decodeobj['email']);
						if(empty($uniqeid))
						{
								$data1=array('name'=>$decodeobj['name'],'phone_no'=>$decodeobj['phone_no'],'address'=>$decodeobj['address'],'city'=>$decodeobj['city'],'zip'=>$decodeobj['zip'],"reg_fee"=>0);
								$res=$this->Api_model->insert_query("buyer",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to register";
									
								}else
								{
									$login_data=array("user_id"=>$res,"user_flag"=>"Buyer","email"=>$decodeobj['email'],"password"=>md5($decodeobj["password"]),"status"=>1,"created_on"=>date('Y-m-d H:i:s'));
									
									$login_id=$this->Api_model->insert_query("login_detail",$login_data);
									if($login_id!=0)
									{
										$buyer_id=$res;
										if(!empty($_FILES))
										{
											//echo json_encode($_FILES,true);die;
												$uploadpath ="uploads";
												$buyerpath="uploads/buyer";
												$buyeridpath="uploads/buyer/".$buyer_id;
												if(!(file_exists($uploadpath)))
												{
													mkdir($uploadpath,0777);
												}	
												if(!(file_exists($buyerpath)))
												{
													mkdir($buyerpath,0777);
												}	
												if(!(file_exists($buyeridpath)))
												{
													mkdir($buyeridpath,0777);
												}
												$result = uploadImg($_FILES,170,188,$buyer_id,$buyeridpath);
												$imgdata=array("licence"=>base_url().''.$buyeridpath.'/'.$result);
												$this->Api_model->update_query("buyer",$buyer_id,$imgdata);
										}
										$email_data=array("name"=>$decodeobj['name'],"flag"=>"Buyer","message"=>"Thank you for signing up with Ezyscrap. !!!");
										$message=$this->registration_email($email_data);
										$msg=$message;
										$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Successfully registered','message'=>$msg);
										$this->Model_basic->sendEmail($data);
										$senddata['code']=200;
										$senddata['status']="success";
										$senddata['description']="Buyer register successfully";	
									}else
									{
										$senddata['code']=406;
										$senddata['status']="Failed";
										$senddata['description']="Failed to register";
									}
								}
						}else
						{
							$senddata['code']=400;
							$senddata['status']="Failed";
							$senddata['description']="This Email id already used";
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	
	public function update_buyer()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				$isExistArr=array("name","phone_no","address","city","zip","token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("name","phone_no","address","city","zip","token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],'Buyer')!='')
						{
								$data1=array('name'=>$decodeobj['name'],'phone_no'=>$decodeobj['phone_no'],'address'=>$decodeobj['address'],'city'=>$decodeobj['city'],'zip'=>$decodeobj['zip']);
								$res=$this->Api_model->update_query("buyer",$this->authentic_user($decodeobj['token'],'Buyer'),$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to update";
								}else
								{
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Customer updated successfully";	
								}
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("email","pwd","flag","device_id");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("email","pwd","flag","device_id");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$res=$this->Api_model->login($decodeobj['email'],md5($decodeobj['pwd']),$decodeobj['flag']);
						
						if(!empty($res))
						{
							if($decodeobj['flag']=='Seller')
							{
								$udata=$this->Api_model->select_query('seller',$res['user_id']);
							}else
							{
								$udata=$this->Api_model->select_query('buyer',$res['user_id']);
							}
							/* for buyer wallet*/
							$wallet_balance=$this->Api_model->get_wallet_balance($res['user_id'],"Buyer");
							$balance=0;
							if(!empty($wallet_balance))
							{
								$balance=$wallet_balance["balance"];
							}
							/*end*/

							$token=$this->generate_token(10);
							$this->Api_model->update_query("login_detail",$res['id'],array("token"=>$token,"device_id"=>$decodeobj['device_id']));
							$senddata['user_info']=array_merge($udata,array("email"=>$decodeobj['email'],"flag"=>$decodeobj['flag'],"wallet_balance"=>$balance));
							$senddata['token']=$token;
							$senddata['code']=200;
							$senddata['status']="success";
							$senddata['description']="Login successfully";  
						}else
						{
							$senddata['code']=208;
							$senddata['status']="Failed";
							$senddata['description']="Please enter valid credential";	
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function change_password()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","flag","old_pwd","new_pwd");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","flag","old_pwd","new_pwd");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
								$res=$this->Api_model->check_is_old_pwd_correct($this->authentic_user($decodeobj['token'],$decodeobj['flag']),$decodeobj['flag'],md5($decodeobj['old_pwd']));
								if(!empty($res))
								{
									$res1=$this->Api_model->update_query("login_detail",$res['id'],array("password"=>md5($decodeobj['new_pwd'])));
									
									if($res1!=0)
									{
										$senddata['code']=200;
										$senddata['status']="success";
										$senddata['description']="Password updated successfully";  
									}else{
										$senddata['code']=208;
										$senddata['status']="Failed";
										$senddata['description']="Something went wrong!please try later";	
									}
									
								}else
								{
									$senddata['code']=208;
									$senddata['status']="Failed";
									$senddata['description']="Old password is incorrect.Please send correct password";	
								}
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
					
	public function add_scrap()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=$this->input->post();
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","type_id","desc","quantity");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","type_id","desc","quantity");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Seller")!='')
						{
					
								$data1=array('seller_id'=>$this->authentic_user($decodeobj['token'],"Seller"),'type_id'=>$decodeobj['type_id'],'desc'=>$decodeobj['desc'],'quantity'=>$decodeobj['quantity'],'status'=>0,'created_on'=>date('Y-m-d H:i:s'));
								$res=$this->Api_model->insert_query("scrap_detail",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to add scrap! Please try later";
									
								}else
								{
									$scrap_id=$res;
									
										$this->Api_model->update_query("scrap_detail",$scrap_id,array('order_id'=>'ORD'.$scrap_id));
									if(!empty($_FILES))
										{
//echo json_encode($_FILES,true);die;
											//echo $scrap_id;die;
												$uploadpath ="uploads";
												$sellerpath="uploads/seller";
												$selleridpath="uploads/seller/".$this->authentic_user($decodeobj['token'],"Seller");
												$scrappath="uploads/seller/".$this->authentic_user($decodeobj['token'],"Seller").'/scrap';
												$scrapidpath="uploads/seller/".$this->authentic_user($decodeobj['token'],"Seller").'/scrap/'.$scrap_id;
												if(!(file_exists($uploadpath)))
												{
													mkdir($uploadpath,0777);
												}	
												if(!(file_exists($sellerpath)))
												{
													mkdir($sellerpath,0777);
												}	
												if(!(file_exists($selleridpath)))
												{
													mkdir($selleridpath,0777);
												}
												if(!(file_exists($scrappath)))
												{
													mkdir($scrappath,0777);
												}
												if(!(file_exists($scrapidpath)))
												{
													mkdir($scrapidpath,0777);
												}
												//$result = uploadImg($_FILES,170,188,$scrap_id,$scrapidpath);
												
												if(!empty($_FILES))
													{
														
														 $filesCount = count($_FILES);
														for($i=1; $i<=$filesCount;$i++)
														{
														//	echo "in";
														$base_path="uploads/test";
														//mkdir($base_path,0777);
														if(!(file_exists($base_path)))
															{
																//echo "hey";
																mkdir($base_path,0777);
															}
													//	echo $base_path;die;
															//echo $_FILES['profileimg']['name'][$i];
															$_FILES['image'.$i]['name'] = $_FILES['image'.$i]['name'];
															$_FILES['image'.$i]['type'] = $_FILES['image'.$i]['type'];
															$_FILES['image'.$i]['tmp_name'] = $_FILES['image'.$i]['tmp_name'];
															$_FILES['image'.$i]['error'] = $_FILES['image'.$i]['error'];
															$_FILES['image'.$i]['size'] = $_FILES['image'.$i]['size'];
															$config['upload_path'] =$scrapidpath;
															$config['allowed_types'] = 'gif|jpg|png|jpeg';
															//echo $_FILES['profileimg']['name'];
															$this->load->library('upload', $config);
															$this->upload->initialize($config);
															if($this->upload->do_upload('image'.$i))
															{
																$img =  $this->upload->data();
																	$imgdata=array("image".$i=>base_url().''.$scrapidpath.'/'.$img['file_name']);
																	$this->Api_model->update_query("scrap_detail",$scrap_id,$imgdata);
															}else
															{
																$error = array('error' => $this->upload->display_errors());
																echo json_encode($error,true);die;
																$senddata['code']=406;
																$senddata['status']="Success";
																$senddata['description']="Your scrap detail added successfully! while uploading images there is somethingwent wrong";
															}
														}
													}
											
										}/* else
										{
												$senddata['code']="I'm not getting file.please send image";
											echo json_encode($senddata,true);die;
										} */
										
									
									$senddata['order_id']='ORD'.$scrap_id;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap added successfully";	
								}
							
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function add_scrap_interest()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","scrap_id");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","scrap_id");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Buyer")!='')
						{
					
								$data1=array('buyer_id'=>$this->authentic_user($decodeobj['token'],"Buyer"),'scrap_id'=>$decodeobj['scrap_id'],'status'=>0,'created_on'=>date('Y-m-d H:i:s'));
								$res=$this->Api_model->insert_query("scrap_interest",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Something went wrong!please try later";
									
								}else
								{
									/* $msg="Hello <br> You have successfully added scrap";
									$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Successfully added scrap','senddata'=>$msg);
									$this->Model_basic->sendEmail($data); */
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Your interest has been sent";	
								}
							
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function add_scrap_bidding()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
//print_r($decodeobj);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","scrap_id","bid_amt","gst","total_amt");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","scrap_id","bid_amt","gst","total_amt");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Buyer")!='')
						{
					
								$data1=array('buyer_id'=>$this->authentic_user($decodeobj['token'],"Buyer"),'scrap_id'=>$decodeobj['scrap_id'],'bid_amt'=>$decodeobj['bid_amt'],'gst_amt'=>$decodeobj['gst'],'total_amt'=>$decodeobj['total_amt'],'status'=>0,'sold_status'=>0,'created_on'=>date('Y-m-d H:i:s'));
								$res=$this->Api_model->insert_query("scrap_bid",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Something went wrong!please try later";
									
								}else
								{
									/* $msg="Hello <br> You have successfully added scrap";
									$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Successfully added scrap','senddata'=>$msg);
									$this->Model_basic->sendEmail($data); */
									$this->Api_model->update_query("scrap_bid",$res,array('bid_id'=>'BID'.$res));
									$senddata['bid_id']='BID'.$res;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Bid added successfully";	
								}
							
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function scrap_data_by_id()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","scrap_id","flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","scrap_id","flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
								$scrap_data=$this->Api_model->scrap_data_by_id($decodeobj['scrap_id']);
								
								if(!empty($scrap_data))
								{
										$sold_flag=0;
											if(!empty($sd["status"]==2))
											{
													$sold_flag=1;
													$scrap_sold=$this->Api_model->check_is_scrap_sold_by_me($this->authentic_user($decodeobj['token'],"Buyer"),$scrap_data['id']);
													if(empty($scrap_sold))
													{
														$sold_flag=2;
													}
											}
											$flag=$this->Api_model->check_isinterested($this->authentic_user($decodeobj['token'],"Buyer"),$scrap_data['id']);
											if(!empty($flag))
											{
												$flag="1";
											}else
											{
												$flag="0";
											}
											
									$is_bid=$this->Api_model->check_is_bid_aded($this->authentic_user($decodeobj['token'],"Buyer"),$scrap_data['id']);
									//print_r($bid);die;
											if(!empty($is_bid))
											{
													$bid="1";
													$bit_amt=$is_bid["bid_amt"];
											}else
											{
													$bid="0";
													$bit_amt=00;
											}
												$final_data=array_merge($scrap_data,array("flag"=>$flag,"bid"=>$bid,"sold_flag"=>$sold_flag,"bid_amt"=>$bit_amt));
									$senddata['scrap_data']=$final_data;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap data";	
								}
								else
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Data is not available";
								}
								//print_r($scrap_data);die;
							}
						}
					}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function all_notifications()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
							$result=$this->Api_model->get_all_notifications($this->authentic_user($decodeobj['token'],$decodeobj['flag']),$decodeobj['flag']);
							if(empty($result))
							{
								$senddata['code']=406;
								$senddata['status']="Failed";
								$senddata['description']="No data available";
							}else
							{
								$senddata['notif_data']=$result;
								$senddata['code']=200;
								$senddata['status']="success";
								$senddata['description']="notification data";	
							}
						}
					}
				}
			}
			else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function order_history()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Buyer")!='')
						{
								$scrap_data=$this->Api_model->get_order_data($this->authentic_user($decodeobj['token'],"Buyer"));
								
								
								if(!empty($scrap_data))
								{
									$scrap=array();
									foreach($scrap_data as $sd)
									{
										$bid_per_kg=$this->Api_model->get_bid_data($this->authentic_user($decodeobj['token'],"Buyer"),$sd['id']);
										$scrap[]=array_merge($sd,array('bid_per_kg'=>$bid_per_kg['bid_amt']));
									}
									$senddata['scrap_data']=$scrap;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap data";	
								}
								else
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Data is not available";
								}
								//print_r($scrap_data);die;
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function payment_history()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Buyer")!='')
						{
								$scrap_data=$this->Api_model->get_payment_data($this->authentic_user($decodeobj['token'],"Buyer"));
								$wallet_balance=$this->Api_model->get_wallet_balance($this->authentic_user($decodeobj['token'],"Buyer"),"Buyer");
								$balance="";
								if(!empty($wallet_balance))
								{
									$balance=$wallet_balance["balance"];
								}
								if(!empty($scrap_data))
								{
									$scrap=array();
									foreach($scrap_data as $sd)
									{
										$bid_per_kg=$this->Api_model->get_bid_data($this->authentic_user($decodeobj['token'],"Buyer"),$sd['id']);
										$scrap[]=array_merge($sd,array('bid_per_kg'=>$bid_per_kg['bid_amt']));
									}
								}
								if(!empty($scrap))
								{
									$senddata['payment_data']=$scrap;
									$senddata['wallet_balance']=$balance;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap data";	
								}
								else
								{
									$senddata['wallet_balance']=$balance;
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Data is not available";
								}
							
							}
						}
					}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function buyer_home_screen()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Buyer")!='')
						{
								$scrap_data=$this->Api_model->get_scrap_data();
								$final_data=array();
								$final_array=array();
								$buyer_data=$this->Api_model->select_query("buyer",$this->authentic_user($decodeobj['token'],"Buyer"));
								
								if(!empty($scrap_data))
								{
									//print_r($scrap_data);die;
									foreach($scrap_data as $sd)
									{
											//$scrap_sold=$this->Api_model->check_is_scrap_sold($sd['id']);
											//print_r($scrap_sold);die;
											$sold_flag=0;
											if(!empty($sd["status"]==2))
											{
													$sold_flag=1;
													$scrap_sold=$this->Api_model->check_is_scrap_sold_by_me($this->authentic_user($decodeobj['token'],"Buyer"),$sd['id']);
													if(empty($scrap_sold))
													{
														$sold_flag=2;
													}
											}
											$flag=$this->Api_model->check_isinterested($this->authentic_user($decodeobj['token'],"Buyer"),$sd['id']);
											if(!empty($flag))
											{
												$flag="1";
											}else
											{
												$flag="0";
											}
											
									$is_bid=$this->Api_model->check_is_bid_aded($this->authentic_user($decodeobj['token'],"Buyer"),$sd['id']);
									//print_r($bid);die;
											if(!empty($is_bid))
											{
													$bid="1";
													$bit_amt=$is_bid["bid_amt"];
											}else
											{
													$bid="0";
													$bit_amt=00;
											}
												$final_data[]=array_merge($sd,array("flag"=>$flag,"bid"=>$bid,"sold_flag"=>$sold_flag,"bid_amt"=>$bit_amt));
									}
									$wallet_balance=$this->Api_model->get_wallet_balance($this->authentic_user($decodeobj['token'],"Buyer"),"Buyer");
									$balance="";
									if(!empty($wallet_balance))
									{
										$balance=$wallet_balance["balance"];
									}
									$senddata['scrap_data']=$final_data;
									$senddata['wallet_balance']=$balance;
									$senddata['reg_fee']=$buyer_data["reg_fee"];
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap data";	
								}
								else
								{
									$senddata['wallet_balance']=$balance;
									$senddata['reg_fee']=$buyer_data["reg_fee"];
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Data is not available";
								}
								//print_r($scrap_data);die;
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function seller_home_screen()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("token");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],"Seller")!='')
						{
								$scrap_data=$this->Api_model->get_seller_scrap_data($this->authentic_user($decodeobj['token'],"Seller"));
								$scrap_type=$this->Api_model->get_scrap_type();
								$sold_scrap=array();
								$live_scrap=array();
								if(!empty($scrap_data))
								{
									foreach($scrap_data as $sd)
									{
										//	unlink($sd['min_amt']);
											$ids=$this->Api_model->sold_scrap($sd['id']);
											if(!empty($ids))
											{
												$sold_scrap[]=array_merge($sd,array("buyer_name"=>$ids["name"]));
											}else
											{
													$live_scrap[]=$sd;
											}
									}
									$senddata['scrap_type']=$scrap_type;
									$senddata['sold_scrap']=$sold_scrap;
									$senddata['live_scrap']=$live_scrap;
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Scrap data";	
								}
								else
								{
									$senddata['scrap_type']=$scrap_type;
									$senddata['sold_scrap']=$sold_scrap;
									$senddata['live_scrap']=$live_scrap;
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Data is not available";
								}
								//print_r($scrap_data);die;
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}

	public function forgot_password()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("email","flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("email","flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$res=$this->Api_model->forgot_password($decodeobj['email'],$decodeobj['flag']);
						//print_r($res);die;
						if(empty($res))
						{
							$senddata['code']=406;
							$senddata['status']="Failed";
							$senddata['description']="Please send valid email id.";
						}else
						{
							$new_pwd=$this->generate_token(8);
							$pwd=array("password"=>md5($new_pwd));
							$result=$this->Api_model->update_query("login_detail",$res['id'],$pwd);
							
							if($result!=0)
							{
								$senddata['new_password']=$new_pwd;
								$senddata['code']=200;
								$senddata['status']="success";
								$senddata['description']="new password generated";
								$user_data=$this->Api_model->get_user_data($res['id'],$decodeobj['flag']);
										$email_data=array("name"=>$user_data['name'],"flag"=>$decodeobj['flag'],"message"=>"Your password has been changed.</br></br> 
										New password is:".$new_pwd);
										$message=$this->registration_email($email_data);
										$msg=$message;
										$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$user_data['email'],'from_name'=>'EzyScrap','subject'=>'Successfully payment Done','message'=>$msg);
										//$this->Model_basic->sendEmail($data);
								//$msg="Hello  <br> Your password has been change.
								//new password is:".$new_pwd;
								$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Reset Password Confirmation','message'=>$msg,);
								$this->Model_basic->sendEmail($data);								
							}else{
								$senddata['code']=406;
								$senddata['status']="Failed";
								$senddata['description']="Opps!Something went wrong";
							}
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function save_registration_payment_detail()
	{
		if($_SERVER['REQUEST_METHOD']==='POST')
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("token","flag","CHECKSUMHASH","BANKNAME","ORDERID","TXNAMOUNT","TXNDATE","TXNID");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{ 
					$issetArr=array("token","flag","CHECKSUMHASH","ORDERID","TXNAMOUNT","TXNDATE","TXNID");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
							$payment_data=array("user_id"=>$this->authentic_user($decodeobj['token'],$decodeobj['flag']),"checksumhash"=>$decodeobj['CHECKSUMHASH'],"bank_name"=>$decodeobj['BANKNAME'],"order_id"=>$decodeobj['ORDERID'],"txn_amt"=>$decodeobj['TXNAMOUNT'],"txn_date"=>$decodeobj['TXNDATE'],"txn_id"=>$decodeobj['TXNID'],"status"=>1,"created_on"=>date("Y-m-d H:i:s"),"reg_status"=>1);
							$payment_id=$this->Api_model->insert_query("payment_detail",$payment_data);
							if($payment_id!=0)
							{
									$result=$this->Api_model->update_query("buyer",$this->authentic_user($decodeobj['token'],$decodeobj['flag']),array("reg_fee"=>1));
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="your payment has been successfully done";
								}else
								{
									$senddata['code']=208;
									$senddata['status']="Failed";
									$senddata['description']="something went wrong!";
								}
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		echo json_encode($senddata,true);die;
	}
	public function save_payment_detail()
	{
		if($_SERVER['REQUEST_METHOD']==='POST')
		{
			$data1 = file_get_contents("php://input");
			$decodeobj=json_decode($data1,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("token","flag","ORDERID","reg_flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{ 
					$issetArr=array("token","flag","ORDERID","reg_flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
							header("Pragma: no-cache");
							header("Cache-Control: no-cache");
							header("Expires: 0");
							require_once APPPATH."third_party/paytm_lib/config_paytm.php";
							require_once APPPATH."third_party/paytm_lib/encdec_paytm.php";
							$checkSum = "";    
							$data = array(
							"MID"=>"SHRGAJ95090720260165",
							"ORDER_ID"=>$decodeobj['ORDERID'],
							);
							$key = 'J8Fa0l5ELf6nszgX';
							$checkSum =getChecksumFromArray($data, $key);
						
							$request=array("MID"=>'SHRGAJ95090720260165',"ORDERID"=>$decodeobj['ORDERID'],"CHECKSUMHASH"=>$checkSum);
							//print_r($request);die;
							$JsonData =json_encode($request);
							$postData = 'JsonData='.urlencode($JsonData);
							$url = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/getTxnStatus";
			//print_r($postData);die;
							$HEADER[] = "Content-Type: application/json";
							$HEADER[] = "Accept: application/json";

							$args['HEADER'] = $HEADER;  
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL,$url);
							curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);	
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_HTTPHEADER, $args['HEADER']);
							$server_data = curl_exec($ch);
							$server_output=json_decode($server_data,true);
							//print_r($server_output);die;
							if($server_output['STATUS']=='TXN_SUCCESS')
							{
								if($decodeobj['reg_flag']==0)
								{
									$payment_data=array("user_id"=>$this->authentic_user($decodeobj['token'],$decodeobj['flag']),"checksumhash"=>$checkSum,"bank_name"=>$server_output['BANKNAME'],"order_id"=>$server_output['ORDERID'],"txn_amt"=>$server_output['TXNAMOUNT'],"txn_date"=>$server_output['TXNDATE'],"txn_id"=>$server_output['TXNID'],"status"=>1,"created_on"=>date("Y-m-d H:i:s"),"reg_status"=>0);
									$payment_id=$this->Api_model->insert_query("payment_detail",$payment_data);
									if($payment_id!=0)
									{
										$wallet_data=array("user_id"=>$this->authentic_user($decodeobj['token'],$decodeobj['flag']),"user_flag"=>$decodeobj['flag'],"balance"=>$server_output['TXNAMOUNT'],"status"=>1,"created_on"=>date("Y-m-d H:i:s"));
									
										$wallet_entry=$this->Api_model->update_wallet_balance("user_wallet",$this->authentic_user($decodeobj['token'],$decodeobj['flag']),$decodeobj['flag'],$wallet_data);
										if($wallet_entry!=0)
										{
											$wallet_balance=$this->Api_model->get_wallet_balance($this->authentic_user($decodeobj['token'],"Buyer"),"Buyer");
											$user_data=$this->Api_model->get_user_data($this->authentic_user($decodeobj['token'],"Buyer"),"Buyer");
											
											$email_data=array("name"=>$user_data['name'],"flag"=>"Buyer","message"=>"Your payment has been completed.</br>Transaction id :".$server_output['TXNID']."</br> Amount :".$server_output['TXNAMOUNT']."</br> Wallet balance :".$wallet_balance['balance']);
											$message=$this->registration_email($email_data);
											//$message=$this->email($email_data);
											//$message=$this->invoice_email($email_data);
											$msg=$message;
											$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$user_data['email'],'from_name'=>'EzyScrap','subject'=>'Successfully payment Done','message'=>$msg);
											$this->Model_basic->sendEmail($data);
											
											$senddata['code']=200;
											$senddata['status']="success";
											$senddata['description']="your payment has been successfully done";
										}else
										{
											$senddata['code']=208;
											$senddata['status']="Failed";
											$senddata['description']="payment has been successfully done,something went wrong in wallet transaction";
										}
									}else
									{
											$senddata['code']=208;
											$senddata['status']="Failed";
											$senddata['description']="Something went wrong please try later";
									}
								
								}else
								{
									$payment_data=array("user_id"=>$this->authentic_user($decodeobj['token'],$decodeobj['flag']),"checksumhash"=>$checkSum,"bank_name"=>$server_output['BANKNAME'],"order_id"=>$server_output['ORDERID'],"txn_amt"=>$server_output['TXNAMOUNT'],"txn_date"=>$server_output['TXNDATE'],"txn_id"=>$server_output['TXNID'],"status"=>1,"created_on"=>date("Y-m-d H:i:s"),"reg_status"=>1);
									$payment_id=$this->Api_model->insert_query("payment_detail",$payment_data);
									if($payment_id!=0)
									{
										$result=$this->Api_model->update_query("buyer",$this->authentic_user($decodeobj['token'],$decodeobj['flag']),array("reg_fee"=>1));
											$user_data=$this->Api_model->get_user_data($this->authentic_user($decodeobj['token'],"Buyer"),"Buyer");
										$email_data=array("name"=>$user_data['name'],"flag"=>"Buyer","message"=>"Your payment has been completed.</br>Transaction id :".$server_output['TXNID']."</br> Amount :".$server_output['TXNAMOUNT']);
										$message=$this->registration_email($email_data);
										$msg=$message;
										$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$user_data['email'],'from_name'=>'EzyScrap','subject'=>'Successfully payment Done','message'=>$msg);
										//$this->Model_basic->sendEmail($data);
										$senddata['code']=200;
										$senddata['status']="success";
										$senddata['description']="your payment has been successfully done";
									}else
									{
										$senddata['code']=208;
										$senddata['status']="Failed";
										$senddata['description']="something went wrong!";
									}
								}
							}else
							{
								$senddata['code']=208;
								$senddata['status']="Failed";
								$senddata['description']="Your transaction has been not completed";
							}
							}
							
							
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		echo json_encode($senddata,true);die;
	}
	public function check_array_key_exist($defined,$comming)
	{
		for($i=0;$i<count($defined);$i++)
		{
			if(array_key_exists($defined[$i],$comming))
			{
				$flag=1;
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Send JSON data in proper format";
			//	$senddata['senddata']=$senddata;
				echo json_encode($senddata,true);die;
			}
		}
		if($flag==1)
		{
			return 1;
		}
	}
	public function check_isset_key($arr,$arr2)
	{
		for($i=0;$i<count($arr);$i++)
		{
			$abc=$arr[$i];
			if($arr2[$abc]!='')
			{
				$flag=1;
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="please fill all mandatory field";
				//$senddata['senddata']=$senddata;
				echo json_encode($senddata,true);die;
			}
		}
		if($flag==1)
		{
			return 1;
		}
	}
	 function generate_checksum()
	{
		if($_SERVER['REQUEST_METHOD']==='POST')
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("MID","ORDER_ID","CUST_ID","INDUSTRY_TYPE_ID","CHANNEL_ID","TXN_AMOUNT","WEBSITE","CALLBACK_URL");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{ 
					$issetArr=array("MID","ORDER_ID","CUST_ID","INDUSTRY_TYPE_ID","CHANNEL_ID","TXN_AMOUNT","WEBSITE","CALLBACK_URL");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						
							require_once APPPATH."third_party/paytm_lib/config_paytm.php";
							require_once APPPATH."third_party/paytm_lib/encdec_paytm.php";
							$paramList = array();
							$paramList["MID"] =$decodeobj['MID']; //Provided by Paytm
							$paramList["ORDER_ID"] =$decodeobj['ORDER_ID']; //unique OrderId for every request
							$paramList["CUST_ID"] = $decodeobj['CUST_ID']; // unique customer identifier 
							$paramList["INDUSTRY_TYPE_ID"] = $decodeobj['INDUSTRY_TYPE_ID']; //Provided by Paytm
							$paramList["CHANNEL_ID"] =$decodeobj['CHANNEL_ID']; //Provided by Paytm
							$paramList["TXN_AMOUNT"] = $decodeobj['TXN_AMOUNT']; // transaction amount
							$paramList["WEBSITE"] =$decodeobj['WEBSITE'];//Provided by Paytm
							//$paramList["MOBILE_NO"] =$decodeobj['MOBILE_NO'];//Provided by Paytm
						//	$paramList["EMAIL_ID"] =$decodeobj['EMAIL_ID'];//Provided by Paytm
							$paramList["CALLBACK_URL"] =$decodeobj['CALLBACK_URL'];//Provided by Paytm
							
							//print_r($paramList);
							$checkSum=getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
							//echo $checkSum;die;
							$paramList["CHECKSUMHASH"] = $checkSum;
							//echo json_encode($senddata,true);die;
							$senddata['check_sum_array']=$paramList;
							$senddata['code']=200;
							$senddata['status']="success";
							$senddata['description']="";
							
						
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		echo json_encode($senddata,true);die;
	} 
	public function PaytmTransactionStatus($order_id)
	{

		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");
		require_once APPPATH."third_party/paytm_lib/config_paytm.php";
		require_once APPPATH."third_party/paytm_lib/encdec_paytm.php";

		$checkSum = "";    
		$data = array(
		"MID"=>"SHRGAJ95090720260165",
		"ORDER_ID"=>$order_id,
		);

		$key = '8a%QGMA9aIT4R@7#';
		$checkSum =getChecksumFromArray($data, $key);
		echo $checkSum;die;
		$request=array("MID"=>'SHRGAJ95090720260165',"ORDERID"=>$order_id,"CHECKSUMHASH"=>$checkSum);

		$JsonData =json_encode($request);
		$postData = 'JsonData='.urlencode($JsonData);
		$url = "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus";

		$HEADER[] = "Content-Type: application/json";
		$HEADER[] = "Accept: application/json";

		$args['HEADER'] = $HEADER;  
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $args['HEADER']);
		$server_output = curl_exec($ch);
	
		$data=json_decode($server_output,true);
	
		}
	 function verifyChecksum()
	{
		if($_SERVER['REQUEST_METHOD']==='POST')
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("CHECKSUMHASH");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{ 
					$issetArr=array("CHECKSUMHASH");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						
							require_once APPPATH."third_party/paytm_lib/config_paytm.php";
							require_once APPPATH."third_party/paytm_lib/encdec_paytm.php";
							$paytmChecksum = "";
							$paramList = array();
							$isValidChecksum = FALSE;
							$paramList = $decodeobj;// Array having paytm response
							$paytmChecksum = isset($decodeobj["CHECKSUMHASH"]) ? $decodeobj["CHECKSUMHASH"] : ""; //Sent by Paytm pg
							$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
							//echo $isValidChecksum;
							$senddata['isValidChecksum']=$isValidChecksum;
							$senddata['code']=200;
							$senddata['status']="success";
							$senddata['description']="";
							
						
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		echo json_encode($senddata,true);die;
	} 
	/* function generate_checksum()
	{
		if($_SERVER['REQUEST_METHOD']==='POST')
		{
			
						
		require_once APPPATH."third_party/paytm_lib/config_paytm.php";
		require_once APPPATH."third_party/paytm_lib/encdec_paytm.php";
		$checkSum = "";
			// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
			$paramList = array();
			$paramList["MID"] = 'bitwar88735392848932'; //Provided by Paytm
			$paramList["ORDER_ID"] = '148061'; //unique OrderId for every request
			$paramList["CUST_ID"] = 'CUST0001453'; // unique customer identifier 
			$paramList["INDUSTRY_TYPE_ID"] = 'Retail'; //Provided by Paytm
			$paramList["CHANNEL_ID"] = 'WAP'; //Provided by Paytm
			$paramList["TXN_AMOUNT"] = '10.00'; // transaction amount
			$paramList["WEBSITE"] = 'APP_STAGING';//Provided by Paytm
			$paramList["CALLBACK_URL"] = 'https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp';//Provided by Paytm
			//$paramList["EMAIL"] = 'manthanwar.shrutika@gmail.com'; // customer email id
			//$paramList["MOBILE_NO"] = '9766055207'; // customer 10 digit mobile no.
			$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
			$paramList["CHECKSUMHASH"] = $checkSum;
			print_r($paramList);die;
										
		$senddata['CHECKSUMHASH']=$checkSum;
		//echo json_encode($senddata,true);die;
		$senddata['payt_STATUS']="1";
		$senddata['code']=200;
		$senddata['status']="Success";
		$senddata['description']="";
					
	}
	} */

		function generate_token($length)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$token = '';
			for ($i = 0; $i < $length; $i++) 
			{
				$token .= $characters[rand(0, $charactersLength - 1)];
			}
			return $token;
		}
	public function authentic_user($token,$type)
	{
		$res=$this->Api_model->authentic_user($token,$type);
		if(!empty($res))
		{
			return $res['id'];
		}else{
			$senddata['code']=208;
			$senddata['status']="Failed";
			$senddata['description']="Please enter valid token";
		}
	//	$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function generate_invoice()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				$isExistArr=array("token","flag","scrap_id");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("token","flag","scrap_id");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						if($this->authentic_user($decodeobj['token'],$decodeobj['flag'])!='')
						{
							if($decodeobj['flag']=="Buyer")
							{
								$result=$this->Api_model->get_data_for_invoice($this->authentic_user($decodeobj['token'],$decodeobj['flag']),$decodeobj['scrap_id']);
								if($this->pdf_html($result)!='')
								{
									$senddata['pdf']=$this->pdf_html($result);
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Invoice generated";	
								}
								else
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="please try later";
								}
								
							}
							
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function pdf_invoice($token,$flag,$scrap_id)
	{
		$result["data"]=$this->Api_model->get_data_for_invoice($this->authentic_user($token,$flag),$scrap_id);
	//	print_r($result);die;
		$this->load->view("invoice",$result);
	}
	public function pdf_html($data)
	{
		
		$my_html="<html lang=\"en\">
  <head>
    <meta charset=\"UTF-8\">
    <title>Sample Invoice</title>
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css\">
    <style>
      @import url(http://fonts.googleapis.com/css?family=Bree+Serif);
      body, h1, h2, h3, h4, h5, h6{
      font-family: 'Bree Serif', serif;
      }
    </style>
  </head>
  
  <body>
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-xs-6\">
          <h1>
        
            Logo here
            </a>
          </h1>
        </div>
        <div class=\"col-xs-6 text-right\">
          <h1>INVOICE</h1>
          <h1><small>Invoice #001</small></h1>
        </div>
      </div>
      <div class=\"row\">
        <div class=\"col-xs-5\">
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4>From: <a href=\"#\">Bitware technologies</a></h4>
            </div>
            <div class=\"panel-body\">
              <p>
              601, 6th Floor,<br>
                Wakad Road,White Square Building, <br>
                Hinjewadi, Pune, Maharashtra 411057 <br>
              </p>
            </div>
          </div>
        </div>
        <div class=\"col-xs-5 col-xs-offset-2 text-right\">
          <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
              <h4>To : <a href=\"#\">".$data['name']."</a></h4>
            </div>
            <div class=\"panel-body\">
              <p>
              ".$data['address'].",<br>
                ".$data['city'].",<br>
              ".$data['zip'].",</p>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class=\"table table-bordered\">
        <thead>
          <tr>
            <th>
              <h4>Scrap type</h4>
            </th>
            <th>
              <h4>Description</h4>
            </th>
            <th>
              <h4>Qty</h4>
            </th>
            <th>
              <h4>Rate/Price</h4>
            </th>
            <th>
              <h4>Sub Total</h4>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>".$data['type']."</td>
            <td><a href=\"#\">".$data['desc']."</a></td>
            <td class=\"text-right\">".$data['quantity']."</td>
            <td class=\"text-right\">".round($data['total_amt'])."</td>
            <td class=\"text-right\">".round($data['total_amt'])."</td>
          </tr>
         
        </tbody>
      </table>
      <div class=\"row text-right\">
        <div class=\"col-xs-2 col-xs-offset-8\">
          <p>
            <strong>
            Sub Total : <br>
            GST TAX : <br>
            Total : <br>
            </strong>
          </p>
        </div>
        <div class=\"col-xs-2\">
          <strong>
          ".round($data['total_amt'])." <br>
         ".round($data['gst'])." <br>
          ".round($data['sold_amt'])."<br>
          </strong>
        </div>
      </div>
			 
			
			
			</div>
		  </body>
		</html>";
		//echo $my_html;die;
		$pdf_options = array(
		  "source_type" => 'html',
		  "source" => $my_html,
		  "action" => 'save',
		  "save_directory" =>base_url().'uploads/invoice/',
		  "file_name" =>'Invoice#00'.$data['invoice_id'].'buyer.pdf');
		  
		//  phptopdf($pdf_options);
		  return $my_html;
	}
	public function registration_email($email_data)
	{
		$str='<html>
			<head>
			<title>Ezyscrap</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<style type="text/css">
			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* MEDIA QUERIES */
			@media screen and (max-width: 480px) {
				.mobile-hide {
					display: none !important;
				}
				.mobile-center {
					text-align: center !important;
				}
				.mobile-table {
					width: 100% !important;
				}
				.text-center {
					text-align: center !important;
				}
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }
			</style>
			</head>
			<body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="480" class="mobile-table">
							<tr>
								<td align="center" valign="top" style="font-size:0; padding: 35px;" bgcolor="#ffffff">
								   <img width="300" src="'.base_url().'assets/logo/email.png" />
								</td>
							</tr>
							<tr>
								<td align="center" style="padding: 0px 35px 20px 35px; background-color: #ffffff;" bgcolor="#ffffff">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
												<br>
												<h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;" mc:edit="Headline">
													Hello '.$email_data["name"].',</h2></br></br>

													'.$email_data["message"].'
												
											</td>
										</tr>
									   
							<tr>
								<td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center">                                    
												<img src="'.base_url().'assets/logo/email.png" width="80" height="80" style="display: block; border: 0px;" mc:edit="Logo Footer"/>
											</td>
										</tr>
										<tr>
											<td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;" mc:edit="Address">
												<p style="font-size: 14px; font-weight: 800; line-height: 18px; color: #333333;">
													<strong>Copyright &copy; 2016 <a href="#">Ezyscrap</a>.</strong> All rights reserved.
												</p>
											</td>
										</tr>                            
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</body>
			</html>';

		return $str;
	}
}