<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_controller extends CI_Controller {
	
	function __construct()
	{
		
		parent::__construct();
		$this->load->model("admin/Admin_model");
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper('imgupload');
		
	}
	public function index()
	{
		//echo FCPATH;die;
		$this->after_session();
			$data['scrap']=$this->Admin_model->get_count('scrap_detail');
			$data['buyer']=$this->Admin_model->get_count('buyer');
			$data['seller']=$this->Admin_model->get_count('seller');
			$data['scrap_type']=$this->Admin_model->get_count('scrap_type');
			$data['scrap_data']=$this->Admin_model->get_data_for_scrap('scrap_detail',4);
			$data['buyer_data']=$this->Admin_model->get_data_for_home_screen('buyer',4);
			$data['seller_data']=$this->Admin_model->get_data_for_home_screen('seller',4);
			$data['wallet']=$this->Admin_model->get_admin_wallet_balance();
			$this->load->view('dashboard_view',$data);
	}
	public function buyer_list()
	{
			$this->after_session();
			$buyer=$this->Admin_model->get_all_data('buyer');
				$data=array();
			if(!empty($buyer))
			{
				$data1=array();
				foreach($buyer as $buy)
				{
					$balance=$this->Admin_model->get_wallet_balance($buy["id"]);
					$bal="00";
					if(!empty($balance))
					{
						$bal=$balance["balance"];
					}
					$data1[]=array_merge($buy,array("balance"=>$bal));
				}
				$data["buyer"]=$data1;
			}
			//print_r($data);die;
			$this->load->view('buyer_list_view',$data);
	}
	public function seller_list()
	{
			$this->after_session();
			$data["seller"]=$this->Admin_model->get_all_data('seller');
			$this->load->view('seller_list_view',$data);
	}
	public function add_buyer_view()
	{
			$this->after_session();
			$this->load->view('add_buyer_view');
	}
	public function add_seller_view()
	{
			$this->after_session();
			$this->load->view('add_seller_view');
	}
	public function edit_buyer_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["buyer"]=$this->Admin_model->get_user_info("buyer",$uid);
			$data["email"]=$this->Admin_model->get_login_info("Buyer",$uid);
			$this->load->view('edit_buyer_view',$data);
	}
	public function view_buyer_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["buyer"]=$this->Admin_model->get_user_info("buyer",$uid);
			$data["email"]=$this->Admin_model->get_login_info("Buyer",$uid);
			$this->load->view('view_buyer_view',$data);
	}
	public function edit_seller_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["seller"]=$this->Admin_model->get_user_info("seller",$uid);
			$data["email"]=$this->Admin_model->get_login_info("Seller",$uid);
			$this->load->view('edit_seller_view',$data);
	}
	public function view_seller_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["seller"]=$this->Admin_model->get_user_info("seller",$uid);
			$data["email"]=$this->Admin_model->get_login_info("Seller",$uid);
			$this->load->view('view_seller_view',$data);
	}
	public function edit_seller_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("org_name"=>$this->input->post("org_name"),"phone_no"=>$this->input->post("contact_no"),"address"=>$this->input->post("address"),"city"=>$this->input->post("city"),"zip"=>$this->input->post("zip"));
			$status=$this->Admin_model->update_query("seller",$data,$this->input->post('id'));
			if($status!=0 )
			{
				$login_data=array("email"=>$this->input->post("email"));
					$login_id=$this->Admin_model->update_login_detail("Seller",$login_data,$this->input->post('id'));
					if($login_id!=0)
					{
							$seller_id=$this->input->post('id');
					
						$this->session->set_flashdata("success","User info has successfully updated");
						redirect("admin/Admin_controller/edit_seller_view/".urlencode(base64_encode($seller_id)));
					}else
					{
						$this->session->set_flashdata("error","Something went wrong! please try later");
						redirect("admin/Admin_controller/edit_seller_view/".urlencode(base64_encode($seller_id)));
					}
			}
		}
	}
	public function edit_buyer_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("name"=>$this->input->post("name"),"phone_no"=>$this->input->post("contact_no"),"address"=>$this->input->post("address"),"zip"=>$this->input->post("zip"),"city"=>$this->input->post("city"));
			$status=$this->Admin_model->update_query("buyer",$data,$this->input->post('id'));
			if($status!=0 )
			{
				$login_data=array("email"=>$this->input->post("email"));
					$login_id=$this->Admin_model->update_login_detail("Buyer",$login_data,$this->input->post('id'));
					if($login_id!=0)
					{
							$buyer_id=$this->input->post('id');
						if(!empty($_FILES))
						{
						
								$uploadpath ="uploads";
								$buyerpath="uploads/buyer";
								$buyeridpath="uploads/buyer/".$buyer_id;
								if(!(file_exists($uploadpath)))
								{
									mkdir($uploadpath,0777);
								}	
								if(!(file_exists($buyerpath)))
								{
									mkdir($buyerpath,0777);
								}	
								if(!(file_exists($buyeridpath)))
								{
									mkdir($buyeridpath,0777);
								}
								$result = uploadImg($_FILES,170,188,$buyer_id,$buyeridpath);
								$imgdata=array("licence"=>base_url().''.$buyeridpath.'/'.$result);
								$this->Admin_model->update_query("buyer",$imgdata,$buyer_id);
						}
						$this->session->set_flashdata("success","User info has successfully updated");
						redirect("admin/Admin_controller/edit_buyer_view/".urlencode(base64_encode($buyer_id)));
					}else
					{
						$this->session->set_flashdata("error","Something went wrong! please try later");
						redirect("admin/Admin_controller/edit_buyer_view/".urlencode(base64_encode($buyer_id)));
					}
			}
		}
	}
	public function add_buyer_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("name"=>$this->input->post("name"),"phone_no"=>$this->input->post("contact_no"),"address"=>$this->input->post("address"),"zip"=>$this->input->post("zip"),"city"=>$this->input->post("city"),"reg_fee"=>1);
			$buyer_id=$this->Admin_model->insert_query("buyer",$data);
			if($buyer_id!=0 || $buyer_id!=' ')
			{
				$login_data=array("user_id"=>$buyer_id,"user_flag"=>"Buyer","email"=>$this->input->post("email"),"password"=>md5($this->input->post("password")),"status"=>1,"created_on"=>date('Y-m-d H:i:s'));
					$login_id=$this->Admin_model->insert_query("login_detail",$login_data);
					if($login_id!='' || $login_id!=0)
					{
						if(!empty($_FILES))
						{
								$uploadpath ="uploads";
								$buyerpath="uploads/buyer";
								$buyeridpath="uploads/buyer/".$buyer_id;
								if(!(file_exists($uploadpath)))
								{
									mkdir($uploadpath,0777);
								}	
								if(!(file_exists($buyerpath)))
								{
									mkdir($buyerpath,0777);
								}	
								if(!(file_exists($buyeridpath)))
								{
									mkdir($buyeridpath,0777);
								}
								$result = uploadImg($_FILES,170,188,$buyer_id,$buyeridpath);
								$imgdata=array("licence"=>base_url().''.$buyeridpath.'/'.$result);
								$this->Admin_model->update_query("buyer",$imgdata,$buyer_id);
						}
						
						$this->session->set_flashdata("success","User has successfully registered");
						redirect("admin/Admin_controller/add_buyer_view");
					}else
					{
						$this->session->set_flashdata("error","Something went wrong! please try later");
						redirect("admin/Admin_controller/add_buyer_view");
					}
			}
		}
	}
	public function add_seller_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("org_name"=>$this->input->post("org_name"),"phone_no"=>$this->input->post("contact_no"),"address"=>$this->input->post("address"),"zip"=>$this->input->post("zip"),"city"=>$this->input->post("city"));
			$seller_id=$this->Admin_model->insert_query("seller",$data);
			if($buyer_id!=0 || $buyer_id!=' ')
			{
				$login_data=array("user_id"=>$seller_id,"user_flag"=>"Seller","email"=>$this->input->post("email"),"password"=>md5($this->input->post("password")),"status"=>1,"created_on"=>date('Y-m-d H:i:s'));
					$login_id=$this->Admin_model->insert_query("login_detail",$login_data);
					if($login_id!='' || $login_id!=0)
					{
						$this->session->set_flashdata("success","User has successfully registered");
						redirect("admin/Admin_controller/add_seller_view");
					}else
					{
						$this->session->set_flashdata("error","Something went wrong! please try later");
						redirect("admin/Admin_controller/add_seller_view");
					}
			}
		}
	}
	public function delete_buyer_data()
	{
		$removeid = $this->input->post("id");
		$res=$this->Admin_model->delete_data('buyer',$removeid);
		$this->Admin_model->delete_login_data('Buyer',$removeid);
		if($res==1)
		{
			//echo $this->input->post("img");die;
			if($this->input->post("img")!="00")
			{
				if (file_exists($this->input->post("img")))
				{
					unlink($this->input->post("img"));
				}
			}
			echo "1";die;
			}else{
				echo "0";die;
			}
		
	}
	public function delete_seller_data()
	{
		$removeid = $this->input->post("id");
		$res=$this->Admin_model->delete_data('seller',$removeid);
		$this->Admin_model->delete_login_data('Seller',$removeid);
		if($res==1)
		{
			echo "1";die;
			}else{
				echo "0";die;
			}
		
	}
	public function login()
	{
			$this->load->view('login');
	}
	public function forgot_password()
	{
			$this->load->view('forgotPassword');
	}
	public function is_login()
	{
		if(!empty($this->input->post()))
		{
			$res=$this->Admin_model->is_login($this->input->post('email'),md5($this->input->post('password')));
			if(!empty($res))
			{
				$this->session->set_userdata("admin_info",$res);
				$this->session->set_userdata("admin_id",$res["id"]);
				echo 2;die;
			
			}else
			{
				echo 1;die;
			}
		}
	}
	public function signout($id)
	{
		$aid=base64_decode(urldecode($id));
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_info');
		redirect('admin/Admin_controller/login');
	}
	public function change_password()
	{
		$decodeobj=$this->input->post();
			if(!empty($decodeobj))
			{
				
						$res=$this->Admin_model->forgot_password($decodeobj['email']);
						//print_r($res);die;
						if(empty($res))
						{
								$this->session->set_flashdata("error","Please enter valid email id");
								redirect("admin/Admin_controller/forgot_password");
						}else
						{
							$new_pwd=$this->generate_token(8);
							$pwd=array("password"=>md5($new_pwd));
							$result=$this->Admin_model->update_query("admin",$pwd,$res['id']);
							
							if($result!=0)
							{
								$this->session->set_flashdata("success","Your password has been changed.For new password please check your mail");
								
								$msg="Hello  <br> Your password has been change.
								new password is:".$new_pwd;
								$data = array('from_email' =>'latesheetal@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'MindMap','subject'=>'Change password','message'=>$msg,);
								$this->Model_basic->sendEmail($data);	
								redirect("admin/Admin_controller/forgot_password");								
							}else{
								$this->session->set_flashdata("error","Something went wrong.please try later");
								redirect("admin/Admin_controller/forgot_password");
							}
						}
					
			}else
			{
				$this->session->set_flashdata("error","Please enter email id");
				redirect("admin/Admin_controller/forgot_password");
			}
		
	
	}
	function generate_token($length)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$token = '';
			for ($i = 0; $i < $length; $i++) 
			{
				$token .= $characters[rand(0, $charactersLength - 1)];
			}
			return $token;
		}
	public function after_session()
	{
		if($this->session->userdata('admin_id')=='')
		{
			redirect('admin/Admin_controller/login');
		}
	}
	public function update_user_password()
	{
		$buyer_id = $this->input->post("id");
		$new_password = $this->input->post("password");
		$flag = $this->input->post("flag");

		//Get uid
		$login_data = $this->Admin_model->get_user_id($buyer_id, $flag);
		
		$data = array(
			'password' => md5($new_password),
			'updated_on' => date('Y-m-d H:i:s')
		);
		// die;
		$result = $this->Admin_model->update_query('login_detail',$data, $login_data->id);
	
		if($result!=0 )
		{
			echo 1;
		} else {
			echo 0;
		}
	}
}