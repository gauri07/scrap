<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Scrap_controller extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("admin/Scrap_model");
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper('imgupload');
		$this->load->model('Model_basic');
	}
	/* public function send_mail()
	{
		$msg='testing mail';
		$data = array('from_email' =>'djgajananenterprises@gmail.com','to_email'=>'latesheetal@gmail.com','from_name'=>'ScrapApp','subject'=>'Successfully approved bid','message'=>$msg);
		$this->Model_basic->sendEmail($data);
	} */
	public function index()
	{
		$this->after_session();
		$data["scrap"]=$this->Scrap_model->get_all_scrap_data();
	//print_r($data["scrap"]);die;
		$this->load->view('scrap_list_view',$data);
	}
	public function sold_scrap()
	{
		$this->after_session();
		$data["scrap"]=$this->Scrap_model->get_all_sold_scrap_data();
	//print_r($data["scrap"]);die;
		$this->load->view('sold_scrap_view',$data);
	}
	public function all_interests()
	{
		$this->after_session();
		$data["scrap"]=$this->Scrap_model->get_all_scrap_interest_data();
	//	print_r($data["scrap"]);die;
		$this->load->view('scrap_interest_view',$data);
	}
	public function add_scrap()
	{
		$this->after_session();
		$data["scrap_type"]=$this->Scrap_model->get_all_scrap_type();
		$data["seller"]=$this->Scrap_model->get_all_seller();
		$this->load->view('add_scrap_view',$data);
	}
	public function add_scrap_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("type_id"=>$this->input->post("scrap_type"),"seller_id"=>$this->input->post("seller_id"),"desc"=>$this->input->post("desc"),"quantity"=>$this->input->post("qty").' '.$this->input->post("unit"),'status'=>0,'created_on'=>date('Y-m-d H:i:s'));
			$scrap_id=$this->Scrap_model->insert_query("scrap_detail",$data);
			if($scrap_id!=0 || $scrap_id!=' ')
			{
				
				$img=array();
				if(!empty($_FILES))
				{
					//print_r($_FILES);die;

						//echo $scrap_id;die;
							$uploadpath ="uploads";
							$sellerpath="uploads/seller";
							$selleridpath="uploads/seller/".$this->input->post("seller_id");
							$scrappath="uploads/seller/".$this->input->post("seller_id").'/scrap';
							$scrapidpath="uploads/seller/".$this->input->post("seller_id").'/scrap/'.$scrap_id;
							if(!(file_exists($uploadpath)))
							{
								mkdir($uploadpath,0777);
							}	
							if(!(file_exists($sellerpath)))
							{
								mkdir($sellerpath,0777);
							}	
							if(!(file_exists($selleridpath)))
							{
								mkdir($selleridpath,0777);
							}
							if(!(file_exists($scrappath)))
							{
								mkdir($scrappath,0777);
							}
							if(!(file_exists($scrapidpath)))
							{
								mkdir($scrapidpath,0777);
							}
							
							 $filesCount = count($_FILES['image']['name']);
					//echo $filesCount;die;
					
					for($i=0; $i<$filesCount;$i++)
					{
						//echo $_FILES['profileimg']['name'][$i];
						$_FILES['image'.$i]['name'] = $_FILES['image']['name'][$i];
						$_FILES['image'.$i]['type'] = $_FILES['image']['type'][$i];
						$_FILES['image'.$i]['tmp_name'] = $_FILES['image']['tmp_name'][$i];
						$_FILES['image'.$i]['error'] = $_FILES['image']['error'][$i];
						$_FILES['image'.$i]['size'] = $_FILES['image']['size'][$i];
						$config['upload_path'] = $scrapidpath;
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						//echo $_FILES['profileimg']['name'];
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('image'.$i))
						{
							//$j=$i+1;
							//$img=$image.''.$j;
							$imga =  $this->upload->data();
							$img[]=base_url().''.$scrapidpath.'/'.$imga['file_name'];
						}
						}
					//	print_r($img);die;
					
						//	$result = uploadImg($_FILES,170,188,$scrap_id,$scrapidpath);
							//echo $result;die;
							
						}
					if (!array_key_exists("0",$img))
					  {
						$img[0]='';
					  }if (!array_key_exists("1",$img))
					  {
						$img[1]='';
					  }if (!array_key_exists("2",$img))
					  {
						$img[2]='';
					  }if (!array_key_exists("3",$img))
					  {
						$img[3]='';
					  }
					$imgdata=array("order_id"=>"ORD".$scrap_id,"image1"=>$img[0],"image2"=>$img[1],"image3"=>$img[2],"image4"=>$img[3]);
						//print_r($imgdata);die;	
							$this->Scrap_model->update_query("scrap_detail",$imgdata,$scrap_id);
							//echo 1;die;
					$this->session->set_flashdata("success","Scrap added successfully");
					redirect("admin/Scrap_controller/add_scrap");
				}else
				{
					//echo 0;die;
					$this->session->set_flashdata("error","Something went wrong! please try later");
					redirect("admin/Scrap_controller/add_scrap");
				}
		}
	}
	public function scrap_type_view()
	{
			$this->after_session();
			$data["scrap_type"]=$this->Scrap_model->get_all_scrap_type();
		
			$this->load->view('scrap_type_view',$data);
	}
	public function change_type_status()
	{
		if(!empty($this->input->post()))
		{
			
			$data=array('status'=>$this->input->post('status1',true));
			//echo  $this->input->post('status1');die;
			$res=$this->Scrap_model->update_query("scrap_type",$data,$this->input->post('id'));
			//echo $res;die;
			if($res==1)
			{
				echo 1;die;
			}else{
				echo 0;die;
			}
		}
	}
	public function delete_data()
	{
		$removeid = $this->input->post("id");
		$res=$this->Scrap_model->delete_data("scrap_type",$removeid);
		
			if($res==1)
			{
				echo "1";die;
			}else{
				echo "0";die;
			}
		
	}
	public function save_image()
	{
				if(!empty($_FILES))
					{
						$scrap_id=$this->input->post('id');
						//echo $scrap_id;die;
							$uploadpath ="uploads";
							$sellerpath="uploads/seller";
							$selleridpath="uploads/seller/".$this->input->post("seller_id");
							$scrappath="uploads/seller/".$this->input->post("seller_id").'/scrap';
							$scrapidpath="uploads/seller/".$this->input->post("seller_id").'/scrap/'.$scrap_id;
							if(!(file_exists($uploadpath)))
							{
								mkdir($uploadpath,0777);
							}	
							if(!(file_exists($sellerpath)))
							{
								mkdir($sellerpath,0777);
							}	
							if(!(file_exists($selleridpath)))
							{
								mkdir($selleridpath,0777);
							}
							if(!(file_exists($scrappath)))
							{
								mkdir($scrappath,0777);
							}
							if(!(file_exists($scrapidpath)))
							{
								mkdir($scrapidpath,0777);
							}
							$result = uploadImg($_FILES,170,188,$scrap_id,$scrapidpath);
							//echo $result;die;
							$imgdata=array("image".$this->input->post("flag")=>base_url().''.$scrapidpath.'/'.$result);
							$res11=$this->Scrap_model->update_query("scrap_detail",$imgdata,$scrap_id);
							if($res11==1)
							{
								echo 1;die;
							}else
							{
								echo 0;die;
							}
							
					}
	}
	public function delete_image()
	{
			$res=$this->Scrap_model->update_query("scrap_detail",array("".$this->input->post('name').""=>""),$this->input->post('scrap_id'));
			if($res==1)
			{
				echo 1;die;
			}else
			{
				echo 0;die;
			}
	}
	public function edit_scrap_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["scrap_type"]=$this->Scrap_model->get_all_scrap_type();
			$data["seller"]=$this->Scrap_model->get_all_seller();
			//echo $uid;die;
			$data["scrap"]=$this->Scrap_model->scrap_data_by_id($uid);
		//	print_r($data);die;
			$this->load->view('edit_scrap_view',$data);
	}
	public function view_scrap_view($id)
	{
			$this->after_session();
		$uid=base64_decode(urldecode($id));
			$data["scrap_type"]=$this->Scrap_model->get_all_scrap_type();
			$data["seller"]=$this->Scrap_model->get_all_seller();
			$data["scrap"]=$this->Scrap_model->scrap_data_by_id($uid);
			$data["scrap_id"]=$uid;
			$this->load->view('view_scrap_view',$data);
	}
	public function edit_scrap_info()
	{
		if(!empty($this->input->post()))
		{
			$data=array("seller_id"=>$this->input->post("seller_id"),"type_id"=>$this->input->post("type_id"),"desc"=>$this->input->post("desc"),"quantity"=>$this->input->post("quantity").' '.$this->input->post("unit"),"min_amt"=>$this->input->post("min_amt").' '.$this->input->post("unit1"),"gst"=>$this->input->post("gst"),"seller_amt_per"=>$this->input->post("seller_amt"));
			$status=$this->Scrap_model->update_query("scrap_detail",$data,$this->input->post('id'));
			$scrap_id=$this->input->post('id');
			if($status!=0 )
			{
					$this->session->set_flashdata("success","User info has successfully updated");
					redirect("admin/Scrap_controller/edit_scrap_view/".urlencode(base64_encode($scrap_id)));
				}else
				{
					$this->session->set_flashdata("error","Something went wrong! please try later");
					redirect("admin/Admin_controller/edit_scrap_view/".urlencode(base64_encode($scrap_id)));
				}
			}
		}
	
	public function delete_scrap_data()
	{
		$removeid = $this->input->post("id");
		$res=$this->Scrap_model->delete_data('scrap_detail',$removeid);
		if($res==1)
		{
			//echo $this->input->post("img");die;
			if($this->input->post("img")!="00")
			{
				if (file_exists($this->input->post("img")))
				{
					unlink($this->input->post("img"));
				}
			}
			echo "1";die;
			}else{
				echo "0";die;
			}
		
	}
	public function all_bid()
	{
		$this->after_session();
		$data["scrap"]=$this->Scrap_model->get_all_bid_data();
	//	print_r($data["scrap"]);die;
		$this->load->view('bid_notif_view',$data);
	}
	public function approve_scrap()
	{
		if($this->input->post("unit")=="kg")
		{
			$min_amt=$this->input->post("bid_amt");
		}elseif($this->input->post("unit")=="ql")
		{
				$min_amt=$this->input->post("bid_amt")/100;
		}elseif($this->input->post("unit")=="ton")
		{
				$min_amt=$this->input->post("bid_amt")/1000;
		}
		$data=array("min_amt"=>$this->input->post("bid_amt")." ".$this->input->post("unit"),"gst"=>$this->input->post("gst"),"seller_amt_per"=>$this->input->post("seller_amt"),"status"=>1);
		//print_r($data);die;
		$res=$this->Scrap_model->update_query("scrap_detail",$data,$this->input->post("scrap_id"));
		$seller_id=$this->Scrap_model->get_seller_id($this->input->post("scrap_id"));
		//echo $res;die;
		if($res!=0)
		{
			$ids=$this->Scrap_model->get_buyer_deviceids();
				if(!empty($ids))
				{
					$device_id=array();
					$msg="New scrap is available";
					foreach($ids as $id)
					{
						$device_id[]=$id['device_id'];
						$this->Scrap_model->insert_query("notifications",array("user_id"=>$id['user_id'],"user_type"=>"Buyer","notification"=>"New scrap is available","status"=>1,"created_on"=>date("Y-m-d H:i:s")));
					}
				}
				
				$dataArr['device_id'] =$device_id;//fcm device ids array which is created previously
				$dataArr['message'] ="scrap_approvewwwScrapwwwNew scrap is availablewww".$this->input->post("scrap_id");
				$dataArr['title'] ="Scrap";
				//print_r($dataArr);die;
				$result =sendNotification($dataArr);
				//print_r($seller_id['device_id']);die;
				$s_id=array();
				$s_id[]=$seller_id['device_id'];
				$arr['device_id'] =$s_id;//fcm device ids array which is created previously
				$arr['message'] ="scrap_approve_sellerwwwScrapwwwHello ".$seller_id['org_name']." Your scrap has been approved, Now it is available for sale on Ezyscrap.";
				$arr['title'] ="Scrap";
				//print_r($dataArr);die;
				$result1 =sendNotification($arr);
				$this->Scrap_model->insert_query("notifications",array("user_id"=>$seller_id['seller_id'],"user_type"=>"Seller","notification"=>"Hello ".$seller_id['org_name']." Your scrap has been approved, Now it is available for sale on Ezyscrap.","status"=>1,"created_on"=>date("Y-m-d H:i:s")));
				//var_dump($result1);die;
			echo 1;die;
		}else
		{
			echo 0;die;
		}
	}
	public function approve_bid()
	{
		$check_status=$this->Scrap_model->check_status($this->input->post("bid_id"));
		if(!empty($check_status))
		{
			echo 2;die;
		}
		$data=array("sold_status"=>1);
		$res=$this->Scrap_model->update_sold_status("scrap_bid",array("sold_status"=>2),$this->input->post("scrap_id"));
		$res=$this->Scrap_model->update_query("scrap_bid",$data,$this->input->post("bid_id"));
		$this->Scrap_model->update_query("scrap_detail",array("status"=>2),$this->input->post("scrap_id"));
			
			
		if($res!=0)
		{
			$result=$this->Scrap_model->bid_data($this->input->post("bid_id"));
			$abc=preg_split("/[a-zA-Z]/", "".$result["quantity"]."");
		//print_r($abc);die;
			$total_amt=$result["total_amt"]-$result["gst_amt"];
				$amt=$result["total_amt"];
			$data1=array("scrap_id"=>$this->input->post("scrap_id"),"buyer_id"=>$result["buyer_id"],"sold_amt"=>$result["total_amt"],"total_amt"=>$total_amt,"gst"=>$result["gst_amt"],"status"=>1,"created_on"=>date("Y-m-d H:i:s"));
			$insert_res=$this->Scrap_model->insert_query("scrap_payment_detail",$data1);
		
			
			if($insert_res!=0)
			{
				$res=$this->Scrap_model->update_wallet($result["buyer_id"],$result["total_amt"]);
				$wallet_amt=$this->Scrap_model->get_wallet_balance($result["buyer_id"]);
				$id=$this->Scrap_model->get_deviceid($result["buyer_id"]);
				//print_r($id);die;
				if(!empty($id))
				{
						$email_data=$this->Scrap_model->get_data_for_invoice($result["buyer_id"],$this->input->post("scrap_id"));
						$message=$this->email($email_data);
						//$message=$this->invoice_email($email_data);
						$msg=$message;
					$data = array('from_email' =>'djgajananenterprises@gmail.com', 'to_email'=>$id['email'],'from_name'=>'ScrapApp','subject'=>'Successfully approved bid','message'=>$msg);
					$this->Model_basic->sendEmail($data);
					//$device_id=array();
						//$device_id[]=$id['device_id'];
						//$this->Scrap_model->insert_query("notifications",array("user_id"=>$id['user_id'],"user_type"=>"Buyer","notification"=>"Bid is approved, ".round($amt,2)."Rs. has been deducted from your account.","status"=>1,"created_on"=>date("Y-m-d H:i:s")));
						$this->Scrap_model->insert_query("notifications",array("user_id"=>$id['user_id'],"user_type"=>"Buyer","notification"=>"Bid is approved, ".round($amt,2)."Rs. has been deducted from your account.","status"=>1,"created_on"=>date("Y-m-d H:i:s")));
					
				$device_id=array();
				$device_id[]=$id['device_id'];
				$dataArr['device_id'] =$device_id;//fcm device ids array which is created previously
				//$dataArr['message'] ="bid_approvewwwScrapwwwCongratulations your Bid is approved.".round($amt,2)."Rs. has been deducted from your account.www".round($wallet_amt['balance'],2);
				$dataArr['message'] ="bid_approvewwwScrapwwwCongratulations your Bid is approved.".round($amt,2)."Rs. has been deducted from your account.www".round($wallet_amt['balance'],2);


				$dataArr['title'] ="Scrap";
		//print_r($dataArr);die;
				$result =sendNotification($dataArr);
				}
				echo 1;die;
			}else
			{
				echo 0;die;
			}
			
		}else
		{
			echo 0;die;
		}
	}
	public function change_status()
	{
		$data=array("status"=>1);
		$res=$this->Scrap_model->update_query("scrap_interest",$data,$this->input->post("id"));
		if($res!=0)
		{
			echo 1;die;
		}else
		{
			echo 0;die;
		}
	}
		public function change_bid_status()
	{
		$data=array("status"=>1);
		$res=$this->Scrap_model->update_query("scrap_bid",$data,$this->input->post("id"));
		if($res!=0)
		{
			echo 1;die;
		}else
		{
			echo 0;die;
		}
	}
	public function edit_scrap_type()
	{
		//print_r($_POST);die;
		$data=array("type"=>$this->input->post("type"));
		if($this->input->post("type_id")!=0)
		{
				$res=$this->Scrap_model->update_query("scrap_type",$data,$this->input->post("type_id"));
		}else
		{
			$res=$this->Scrap_model->insert_query("scrap_type",$data);
		}
	
		if($res!=0)
		{
			echo 1;die;
		}else
		{
			echo 0;die;
		}
	}
	/*
	date: 3rd August 2018
	*/
	public function set_featured_image()
	{
		$id = $this->input->post("id");
		$featured_image = $this->input->post("featured_image");

		$query = $this->Scrap_model->update_featured_image($id, $featured_image);
		if($query)
		{
			echo 1;
		} else {
			echo  0;
		}
	}
	public function after_session()
	{
		if($this->session->userdata('admin_id')=='')
		{
			redirect('admin/Admin_controller/login');
		}
	}
		public function invoice_email($data)
	{
		$str='<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ezyscrap</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="'. base_url().'assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="'. base_url().'assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="'. base_url().'assets/bower_components/Ionicons/css/ionicons.min.css">
  
<link href="'. base_url().'assets/dist/css/dataTables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
    <link href="'. base_url().'assets/dist/css/dataTables/dataTables.responsive.css" rel="stylesheet" type="text/css">
    <link href="'. base_url().'assets/dist/css/dataTables/dataTables.tableTools.min.css" rel="stylesheet" type="text/css">
	 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/form_validation/formValidation.css"/> 
  <!-- Theme style -->
  <link rel="stylesheet" href="'. base_url().'assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="'. base_url().'assets/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Ezyscrap
            <small class="pull-right">Date: <?php echo date("d/m/Y");?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
            <address>
            <strong>Bitware technologies</strong><br>
            601, 6th Floor,<br>
                Wakad Road,White Square Building, <br>
                Hinjewadi, Pune, Maharashtra 411057 <br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>'.$data['name'].'</strong><br>
         '.$data['address'].'<br>
           '.$data['city'].','.$data['zip'].'<br>
            Phone:'.$data['zip'].'<br>
            Email: '.$data['name'].'
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #00'.$data['invoice_id'].'</b><br>
          <br>
          <b>Order ID:</b> '.$data['order_id'].'<br>
          <b>Payment Date:</b>'.date("d/m/Y",strtotime($data['date'])).'<br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Scrap type</th>
              <th>Description</th>
              <th>Qty </th>
              <th>Rate/Price</th>
              <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
               <tr>
            <td>'.$data['type'].'</td>
            <td><a href=\"#\">'.$data['desc'].'</a></td>
            <td class=\"text-right\">'.$data['quantity'].'</td>
            <td class=\"text-right\">'.$data['total_amt'].'</td>
            <td class=\"text-right\">'.$data['total_amt'].'</td>
          </tr>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->

        <!-- /.col -->
        <div class="col-xs-6 col-md-offset-6">
      

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>'.$data['total_amt'].'</td>
              </tr>
              <tr>
                <th>GST</th>
                <td>'.$data['gst'].'</td>
              </tr>
         
              <tr>
                <th>Total:</th>
                <td>'.$data['sold_amt'].'</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
     
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
 

  

<!-- jQuery 3 -->
<script src="'. base_url().'assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="'. base_url().'assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="'. base_url().'assets/dist/js/adminlte.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>';
	}	
	public function email($email_data)
	{
		$str='<html>
			<head>
			<title>Ezyscrap</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<style type="text/css">
			/* CLIENT-SPECIFIC STYLES */
			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
			img { -ms-interpolation-mode: bicubic; }

			/* RESET STYLES */
			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
			table { border-collapse: collapse !important; }
			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

			/* iOS BLUE LINKS */
			a[x-apple-data-detectors] {
				color: inherit !important;
				text-decoration: none !important;
				font-size: inherit !important;
				font-family: inherit !important;
				font-weight: inherit !important;
				line-height: inherit !important;
			}

			/* MEDIA QUERIES */
			@media screen and (max-width: 480px) {
				.mobile-hide {
					display: none !important;
				}
				.mobile-center {
					text-align: center !important;
				}
				.mobile-table {
					width: 100% !important;
				}
				.text-center {
					text-align: center !important;
				}
			}

			/* ANDROID CENTER FIX */
			div[style*="margin: 16px 0;"] { margin: 0 !important; }
			</style>
			</head>
			<body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="480" class="mobile-table">
							<tr>
								<td align="center" valign="top" style="font-size:0; padding: 35px;" bgcolor="#ffffff">
								   <img width="300" src="'.base_url().'assets/logo/email.png" />
								</td>
							</tr>
							<tr>
								<td align="center" style="padding: 0px 35px 20px 35px; background-color: #ffffff;" bgcolor="#ffffff">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
												<br>
												<h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;" mc:edit="Headline">
													Hello '.$email_data["name"].',</br>

												Your Bid has been approved!!!
												</h2>
											</td>
										</tr>
										<tr>
									     <table class="table table-striped">
										<thead>
										<tr>
										  <th>Scrap type</th>
										  <th>Description</th>
										  <th>Qty </th>
										  <th>Rate/Price</th>
										  <th>Sub Total</th>
										</tr>
										</thead>
										<tbody>
										<tr>
									<td>'.$email_data['type'].'</td>
									<td><a href=\"#\">'.$email_data['desc'].'</a></td>
									<td class=\"text-right\">'.$email_data['quantity'].'</td>
									<td class=\"text-right\">'.$email_data['total_amt'].'</td>
									<td class=\"text-right\">'. $email_data['sold_amt'].'</td>
								  </tr>
								   
							</tbody>
						  </table>
						  </tr>
							<tr>
								<td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center">                                    
												<img src="'.base_url().'assets/logo/email.png" width="80" height="80" style="display: block; border: 0px;" mc:edit="Logo Footer"/>
											</td>
										</tr>
										<tr>
											<td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;" mc:edit="Address">
												<p style="font-size: 14px; font-weight: 800; line-height: 18px; color: #333333;">
													<strong>Copyright &copy; 2016 <a href="#">Ezyscrap</a>.</strong> All rights reserved.
												</p>
											</td>
										</tr>                            
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</body>
			</html>';

		return $str;
	}
}