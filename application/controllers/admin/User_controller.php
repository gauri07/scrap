<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'block_io-php-master/lib/block_io.php';

class User_controller extends CI_Controller {
	public $apiKey;
	public $version;
	public $pin;	
	function __construct()
	{
		parent::__construct();
		$this->apiKey = "6521-c960-538d-2dd5";
		$this->version = 2;
		$this->pin = "sheetal1203";
		
		$this->load->model('admin/User_model');
		//$this->load->model('Model_basic');
	}
	public function index()
	{
			$data["users"]=$this->User_model->get_all_user_data();
			$this->load->view('user_list_view',$data);
	}
	public function add_user_view()
	{
	//		$data["users"]=$this->User_model->get_all_user_data();
			$this->load->view('add_user_view');
	}
	public function add_user()
	{
		if(!empty($this->input->post()))
		{
			//print_r($this->input->post());die;
			$mobile=$this->User_model->check_isMobileNoUniqe($this->input->post("contact_no"));
			if(empty($mobile))
			{
				$user_data=array("name"=>$this->input->post("name"),"email"=>$this->input->post("email"),"mobile_no"=>$this->input->post("contact_no"),"address"=>$this->input->post("address"),"city"=>$this->input->post("city"),"state"=>$this->input->post("state"),"pincode"=>$this->input->post("pincode"),"addr_flag"=>1,"status"=>1,"created_on"=>date("Y-m-d H:i:s"));
				$ustatus=$this->User_model->save_user_personal_detail($user_data);
					if($ustatus!=0)
					{
						$bank_data=array("user_id"=>$ustatus,"ifsc_code"=>$this->input->post("ifsc"),"bank_name"=>$this->input->post("bankname"),"branch_name"=>$this->input->post("branchname"),"acc_holder_name"=>$this->input->post("accholdername"),"acc_no"=>$this->input->post("acc_no"),"bank_flag"=>1,"created_on"=>date("Y-m-d H:i:s"));
						$this->User_model->save_user_bank_detail($bank_data);
						$pancard_data=array("user_id"=>$ustatus,"fname"=>$this->input->post("fname"),"lname"=>$this->input->post("lname"),"pancard_no"=>$this->input->post("pan_no"),"bdate"=>$this->input->post("bdate"),"gender"=>$this->input->post("bdate"),"pancard_flag"=>1,"created_on"=>date("Y-m-d H:i:s"));
							$this->User_model->save_user_pancard_detail($pancard_data);
						$res=$this->User_model->check_is_acc_verified($ustatus);
						if(!empty($res))
						{
							if($res['addr_flag']==1 && $res['bank_flag']==1 && $res['pancard_flag']==1)
							{
								$address_generated=$this->User_model->check_is_address_generated($ustatus);
								if(empty($address_generated))
								{
									$block_io = new BlockIo($this->apiKey, $this->pin, $this->version);
									$new_address=$block_io->get_new_address();
									//echo $new_address->data->address;die;
									if(!empty($new_address->status=='success'))
									{
										$final_data=array('user_id'=>$ustatus,'address'=>$new_address->data->address,'network'=>$new_address->data->network,'label'=>$new_address->data->label,'bitcoin_user_id'=>$new_address->data->user_id,'status'=>1,'created_on'=>date('Y-m-d H:i:s'));
										$this->User_model->save_bitcoin_address($final_data);
									}
									else{
								$this->session->set_flashdata("error","error occured in generating address");
								redirect("index.php/admin/User_controller/add_user_view");
							}
								}else{
								$this->session->set_flashdata("success","Address is already generated");
								redirect("index.php/admin/User_controller/add_user_view");
							}
							}else{
								$this->session->set_flashdata("error","");
								redirect("index.php/admin/User_controller/add_user_view");
							}
						}else
						{
							$this->session->set_flashdata("error","");
							redirect("index.php/admin/User_controller/add_user_view");
						}
						
					
				}else
			{
				$this->session->set_flashdata("error","Error occured!! try later");
				redirect("index.php/admin/User_controller/add_user_view");
			}
						//	print_r($user_data);die;
				
			}else
			{
				$this->session->set_flashdata("error","This mobile number is already ");
				redirect("index.php/admin/User_controller/add_user_view");
			}
		}else
		{
			$this->session->set_flashdata("error","Please send data");
			redirect("index.php/admin/User_controller/add_user_view");
		}
	}
	
}