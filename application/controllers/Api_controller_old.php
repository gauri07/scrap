<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Api_model');
		$this->load->model('Model_basic');
		header('Content-Type: application/json');
	}
	public function register_new_seller()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			//print_r($data);die;
			$decodeobj=json_decode($data,true);
			//print_r($decodeobj);die;
		//	echo count($decodeobj['category']);die;
			if(!empty($decodeobj))
			{
				$isExistArr=array("org_name","company_reg_no","email","phone_no","address","city","zip","password");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("org_name","company_reg_no","email","phone_no","address","city","zip","password");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$uniqeid=$this->Api_model->check_isEmailUniqe("Seller",$decodeobj['email']);
						if(empty($uniqeid))
						{
								$data1=array('org_name'=>$decodeobj['org_name'],'company_reg_no'=>$decodeobj['company_reg_no'],'phone_no'=>$decodeobj['phone_no'],'address'=>$decodeobj['address'],'city'=>$decodeobj['city'],'zip'=>$decodeobj['zip']);
								$res=$this->Api_model->insert_query("seller",$data1);
								if($res==0)
								{
									$senddata['code']=406;
									$senddata['status']="Failed";
									$senddata['description']="Failed to register";
									
								}else
								{
									$login_data=array("user_id"=>$res,"user_flag"=>"Seller","email"=>$decodeobj['email'],"password"=>md5($decodeobj["password"]),"status"=>1,"created_on"=>date('Y-m-d H:i:s'));
									
									$login_id=$this->Api_model->insert_query("login_detail",$login_data);
					
									$msg=$decodeobj['org_name']."<br> You have successfully registered";
									$data = array('from_email' =>'latesheetal@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'Successfully registered','senddata'=>$msg);
									//$this->Model_basic->sendEmail($data);
									$senddata['code']=200;
									$senddata['status']="success";
									$senddata['description']="Customer register successfully";	
								}
							
						}else
						{
							$senddata['code']=400;
							$senddata['status']="Failed";
							$senddata['description']="This Email id already used";
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("email","pwd","flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("email","pwd","flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$res=$this->Api_model->login($decodeobj['email'],md5($decodeobj['pwd']),$decodeobj['flag']);
						
						if(!empty($res))
						{
							//print_r($res);die;
							$udata=$this->Api_model->select_query('seller',$res['user_id']);
							
							$token=$this->generate_token(10);
							$this->Api_model->update_query("login_detail",$res['id'],array("token"=>$token));
							$senddata['user_info']=array_merge($udata,array("email"=>$decodeobj['email'],"flag"=>$decodeobj['flag']));
							$senddata['token']=$token;
							$senddata['code']=200;
							$senddata['status']="success";
							$senddata['description']="Login successfully";  
						}else
						{
							$senddata['code']=208;
							$senddata['status']="Failed";
							$senddata['description']="Please enter valid credential";	
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
	public function forgot_password()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') 
		{
			$data = file_get_contents("php://input");
			$decodeobj=json_decode($data,true);
			if(!empty($decodeobj))
			{
				//print_r($decodeobj);die;
				$isExistArr=array("email","flag");
				if($this->check_array_key_exist($isExistArr,$decodeobj)==1) 
				{
					$issetArr=array("email","flag");
					if($this->check_isset_key($issetArr,$decodeobj)==1)
					{
						$res=$this->Api_model->forgot_password($decodeobj['email'],$decodeobj['flag']);
						//print_r($res);die;
						if(empty($res))
						{
							$senddata['code']=406;
							$senddata['status']="Failed";
							$senddata['description']="Please send valid email id.";
						}else
						{
							$new_pwd=$this->generate_token(8);
							$pwd=array("password"=>md5($new_pwd));
							$result=$this->Api_model->update_query("login_detail",$res['id'],$pwd);
							
							if($result!=0)
							{
								$senddata['new_password']=$new_pwd;
								$senddata['code']=200;
								$senddata['status']="success";
								$senddata['description']="new password generated";
								
								$msg="Hello  <br> Your password has been change.
								new password is:".$new_pwd;
								$data = array('from_email' =>'late.sheetal0@gmail.com', 'to_email'=>$decodeobj['email'],'from_name'=>'ScrapApp','subject'=>'','message'=>$msg,);
								$this->Model_basic->sendEmail($data);								
							}else{
								$senddata['code']=406;
								$senddata['status']="Failed";
								$senddata['description']="Opps!Something went wrong";
							}
						}
					}
				}
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Please Send JSON data";
			}
		}else
		{
			$senddata['code']=211;
			$senddata['status']="Failed";
			$senddata['description']="This API accpets POST Method only.";  	
		}
		//$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
		public function check_array_key_exist($defined,$comming)
	{
		for($i=0;$i<count($defined);$i++)
		{
			if(array_key_exists($defined[$i],$comming))
			{
				$flag=1;
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="Send JSON data in proper format";
			//	$senddata['senddata']=$senddata;
				echo json_encode($senddata,true);die;
			}
		}
		if($flag==1)
		{
			return 1;
		}
	}
	public function check_isset_key($arr,$arr2)
	{
		for($i=0;$i<count($arr);$i++)
		{
			$abc=$arr[$i];
			if($arr2[$abc]!='')
			{
				$flag=1;
			}else
			{
				$senddata['code']=208;
				$senddata['status']="Failed";
				$senddata['description']="please fill all mandatory field";
				//$senddata['senddata']=$senddata;
				echo json_encode($senddata,true);die;
			}
		}
		if($flag==1)
		{
			return 1;
		}
	}
		function generate_token($length)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$token = '';
			for ($i = 0; $i < $length; $i++) 
			{
				$token .= $characters[rand(0, $charactersLength - 1)];
			}
			return $token;
		}
	public function authentic_user($token)
	{
		$res=$this->Api_model->authentic_user($token);
		if(!empty($res))
		{
			return $res['id'];
		}else{
			$senddata['code']=208;
			$senddata['status']="Failed";
			$senddata['description']="Please enter valid token";
		}
	//	$senddata['senddata']=$senddata;
		echo json_encode($senddata,true);die;
	}
}