<?php
	class User_model extends CI_Model
	{
		function get_user_count()
		{
			$this->db->select("COUNT(id) as user_count");
			$this->db->from("bitcoin_address");
			return $this->db->get()->row_array();
		}
		function get_all_user_data()
		{
			$this->db->select("user.id,name,profile,user.created_on,mobile_no,bitcoin_address.address,user.status,city");
			$this->db->from("user");
			$this->db->join("bitcoin_address","user.id=bitcoin_address.user_id","left");
			$this->db->order_by("user.id","DSE");
			return $this->db->get()->result_array();
		}
		public function check_isMobileNoUniqe($phone)
		{
			$this->db->select('id');
			$this->db->from('user');
			$this->db->where('mobile_no',$phone);
			return $this->db->get()->row_array();
		}
			public function check_is_acc_verified($id)
		{
			$this->db->select('user.id');
			$this->db->from('user');
			$this->db->join('user_bank_detail','user.id=user_bank_detail.user_id','left');
			$this->db->join('user_pancard_detail','user.id=user_bank_detail.user_id','left');
			$this->db->where('user.id',$id);
			$this->db->where('user_bank_detail.user_id',$id);
			$this->db->where('user_pancard_detail.user_id',$id);
			return $this->db->get()->row_array();
		}
		public function check_is_address_generated($uid)
		{
			$this->db->select('id,address');
			$this->db->where('user_id',$uid);
			$this->db->from('bitcoin_address');
			return $this->db->get()->row_array();
		}
		public function save_user_personal_detail($data)
		{
			$this->db->insert('user',$data);
			return $this->db->insert_id();
		}
		public function save_bitcoin_address($data)
		{
			$this->db->insert('bitcoin_address',$data);
			return $this->db->insert_id();
		}
		public function save_user_pancard_detail($data)
		{
			$this->db->insert('user_pancard_detail',$data);
			return $this->db->insert_id();
		}
		public function save_user_bank_detail($data)
		{
			$this->db->insert('user_bank_detail',$data);
			return $this->db->insert_id();
		}
	
	}