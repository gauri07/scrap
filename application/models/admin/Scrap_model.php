<?php
	class Scrap_model extends CI_Model
	{
		function is_login($email,$password)
		{
			$this->db->select("*");
			$this->db->from("admin");
			$this->db->where("email",$email);
			$this->db->where("password",$password);
			return $this->db->get()->row_array();
		}
		function get_all_scrap_data()
		{
			$this->db->select("scrap_detail.*,seller.org_name,scrap_type.type");
			$this->db->from("scrap_detail");
			$this->db->join("seller","scrap_detail.seller_id=seller.id","left");
			$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
			//$this->db->order_by('scrap_detail.status','ASC');
				$this->db->where('scrap_detail.status !=',2);
			$this->db->order_by('scrap_detail.id','DESC');
			return $this->db->get()->result_array();
		}
		function get_all_sold_scrap_data()
		{
			$this->db->select("scrap_detail.*,seller.org_name,seller.id as seller_id,scrap_payment_detail.total_amt,scrap_payment_detail.gst,scrap_payment_detail.sold_amt,scrap_payment_detail.payed_amt,buyer.name,buyer.id as buyer_id");
			$this->db->from("scrap_detail");
			$this->db->join("seller","scrap_detail.seller_id=seller.id","left");
			$this->db->join("scrap_payment_detail","scrap_detail.id=scrap_payment_detail.scrap_id","left");
			$this->db->join("buyer","scrap_payment_detail.buyer_id=buyer.id","left");
			$this->db->where("scrap_detail.status",2);
			$this->db->order_by('scrap_detail.id','DESC');
			return $this->db->get()->result_array();
		}
		function insert_query($table,$data)
		{
			$this->db->insert($table,$data);
			return $this->db->insert_id();
		}
		function get_user_info($table,$id)
		{
				$this->db->select("*");
				$this->db->from($table);
				$this->db->where("id",$id);
				return $this->db->get()->row_array();
		}
		function get_seller_id($sid)
		{
				$this->db->select("scrap_detail.seller_id,login_detail.device_id,seller.org_name");
				$this->db->from('scrap_detail');
				$this->db->join("login_detail","scrap_detail.seller_id=login_detail.user_id");
				$this->db->join("seller","scrap_detail.seller_id=seller.id");
				$this->db->where("scrap_detail.id",$sid);
				$this->db->where("login_detail.user_flag","seller");
				return $this->db->get()->row_array();
		}
		function update_query($table,$data,$id)
		{
			$this->db->where("id",$id);
			return $this->db->update($table,$data);
		}
		function update_sold_status($table,$data,$id)
		{
			$this->db->where("scrap_id",$id);
			return $this->db->update($table,$data);
		}
		function delete_data($table,$id)
		{
			$this->db->where("id",$id);
			return $this->db->delete($table);
		}
		function delete_login_data($user_type,$id)
		{
			$this->db->where("user_id",$id);
			$this->db->where("user_flag",$user_type);
			return $this->db->delete("login_detail");
		}
		function get_login_info($user_type,$id)
		{
			$this->db->select("email");
				$this->db->from("login_detail");
			$this->db->where("user_id",$id);
			$this->db->where("user_flag",$user_type);
			return $this->db->get()->row_array();
		}	
		function check_status($bid_id)
		{
			$this->db->select("sold_status");
			$this->db->from("scrap_bid");
			$this->db->where("id",$bid_id);
			$this->db->where("sold_status",1);
			return $this->db->get()->row_array();
		}
		function get_all_scrap_interest_data()
		{
			$this->db->select("scrap_detail.order_id,scrap_interest.status,buyer.name,scrap_interest.id as interest_id,scrap_interest.scrap_id,buyer.phone_no,buyer.address");
			$this->db->from("scrap_interest");
			$this->db->join("buyer","scrap_interest.buyer_id=buyer.id","left");
			$this->db->join("scrap_detail","scrap_interest.scrap_id=scrap_detail.id","left");
			$this->db->order_by("scrap_interest.id", "DESC");
			return $this->db->get()->result_array();
		}
		function get_all_bid_data()
		{
			$this->db->select("scrap_detail.order_id,scrap_bid.status,scrap_bid.sold_status,buyer.name,scrap_bid.id as bid_id,scrap_bid.scrap_id,buyer.phone_no,buyer.address,scrap_bid.bid_amt");
			$this->db->from("scrap_bid");
			$this->db->join("buyer","scrap_bid.buyer_id=buyer.id","left");
			$this->db->join("scrap_detail","scrap_bid.scrap_id=scrap_detail.id","left");
			$this->db->order_by("scrap_bid.id", "DESC");
			return $this->db->get()->result_array();
		}
		
		function get_all_scrap_type()
		{
			$this->db->select("id,type,status");
			$this->db->from("scrap_type");
			return $this->db->get()->result_array();
		}
		function get_all_seller()
		{
			$this->db->select("id,org_name");
			$this->db->from("seller");
			return $this->db->get()->result_array();
		}
		public function scrap_data_by_id($id)
	{
		$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.desc,scrap_detail.quantity,scrap_detail.featured_image,scrap_detail.image1,scrap_detail.image2,scrap_detail.image3,scrap_detail.image4,scrap_detail.image5,scrap_detail.image6,scrap_detail.image7,scrap_detail.image8,scrap_detail.image9,scrap_detail.image10,scrap_detail.min_amt,scrap_type.type,scrap_detail.type_id,scrap_detail.seller_id,seller.id as seller_id, seller.org_name,scrap_detail.gst,scrap_detail.seller_amt_per,scrap_detail.status');
		$this->db->from('scrap_detail');
			$this->db->join("seller","scrap_detail.seller_id=seller.id","left");
		$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
		$this->db->where('scrap_detail.id',$id);
		return $this->db->get()->row_array();
	}
	public function get_buyer_deviceids()
	{
		$this->db->select('user_id,device_id');
		$this->db->from('login_detail');
		$this->db->where('user_flag','Buyer');
		$this->db->where('status',1);
		return $this->db->get()->result_array();
	}
	public function get_deviceid($uid)
	{
		$this->db->select('user_id,device_id,buyer.name,email');
		$this->db->from('login_detail');
		$this->db->join('buyer','login_detail.user_id=buyer.id','left');
		$this->db->where('user_flag','Buyer');
		$this->db->where('user_id',$uid);
		return $this->db->get()->row_array();
	}
	public function bid_data($bid_id)
	{
		$this->db->select('scrap_bid.*,scrap_detail.quantity');
		$this->db->from('scrap_bid');
		$this->db->join('scrap_detail','scrap_bid.scrap_id=scrap_detail.id','left');
		$this->db->where('scrap_bid.id',$bid_id);
		return $this->db->get()->row_array();
	}
	public function update_wallet($uid,$amt)
	{
			$this->db->where('user_id',$uid);
			$this->db->where('user_flag','Buyer');
			$this->db->set('balance', 'balance - ' .$amt, FALSE);
			$res=$this->db->update("user_wallet");
			if($res!=0)
			{
				$this->db->where('id',1);
				$this->db->set('balance', 'balance + ' .$amt, FALSE);
				$res=$this->db->update("admin_wallet");
			}
	}
	public function get_wallet_balance($uid)
	{
			$this->db->select("balance");
			$this->db->from('user_wallet');
			$this->db->where('user_id',$uid);
			$this->db->where('user_flag','Buyer');
			return $this->db->get()->row_array();
	}
	function get_data_for_invoice($uid,$scrap_id)
		{
			//echo $uid;echo $scrap_id;die;
			
			$this->db->select('scrap_payment_detail.id as invoice_id,scrap_payment_detail.gst,scrap_payment_detail.other_tax,scrap_payment_detail.sold_amt,scrap_payment_detail.total_amt,scrap_detail.quantity,scrap_detail.desc,scrap_type.type,buyer.name,buyer.phone_no,buyer.address,buyer.city,buyer.zip,scrap_detail.order_id,scrap_payment_detail.created_on as date');
			$this->db->from('scrap_payment_detail');
				$this->db->join("scrap_detail","scrap_payment_detail.scrap_id=scrap_detail.id","left");
				$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
				$this->db->join("buyer","scrap_payment_detail.buyer_id=buyer.id","left");
			$this->db->where('scrap_payment_detail.buyer_id',$uid);
			$this->db->where('scrap_payment_detail.scrap_id',$scrap_id);
			return $this->db->get()->row_array();
		}
		/*
		date: 3rd August 2018
		*/
		public function update_featured_image($id, $featured_image)
		{
			$this->db->where("id",$id);
			$this->db->update('scrap_detail', ['featured_image' => $featured_image, 'updated_on' => date('Y-m-d H:i:s')]);
			return ($this->db->affected_rows() != 1) ? false : true;
		}
	}