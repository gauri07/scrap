<?php
	class Admin_model extends CI_Model
	{
		function is_login($email,$password)
		{
			$this->db->select("*");
			$this->db->from("admin");
			$this->db->where("email",$email);
			$this->db->where("password",$password);
			return $this->db->get()->row_array();
		}
		function get_all_data($table)
		{
			$this->db->select("*");
			$this->db->from($table);
				$this->db->order_by("id", "DESC");
			return $this->db->get()->result_array();
		}
		function forgot_password($email)
		{
			$this->db->select("*");
			$this->db->from("admin");
			$this->db->where("email",$email);
			return $this->db->get()->row_array();
		}
		function get_data_for_home_screen($table,$limit)
		{
			$this->db->select("*");
			$this->db->from($table);
				$this->db->order_by('id','DESC');
			$this->db->limit($limit);
			return $this->db->get()->result_array();
		}
		function get_data_for_scrap($table,$limit)
		{
			$this->db->select("scrap_detail.*,seller.org_name,scrap_type.type");
			$this->db->from("scrap_detail");
			$this->db->join("seller","scrap_detail.seller_id=seller.id","left");
			$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
				$this->db->order_by('id','DESC');
			$this->db->limit($limit);
			return $this->db->get()->result_array();
		}
		function get_count($table)
		{
			$this->db->select("COUNT(id) as cnt");
			$this->db->from($table);
			return $this->db->get()->row_array();
		}
		function insert_query($table,$data)
		{
			$this->db->insert($table,$data);
			return $this->db->insert_id();
		}
		function get_user_info($table,$id)
		{
				$this->db->select("*");
				$this->db->from($table);
				$this->db->where("id",$id);
				return $this->db->get()->row_array();
		}
		function update_query($table,$data,$id)
		{
			$this->db->where("id",$id);
			return $this->db->update($table,$data);
		}
		function update_login_detail($flag,$data,$id)
		{
			$this->db->where("user_id",$id);
			$this->db->where("user_flag",$flag);
			return $this->db->update('login_detail',$data);
		}
		function delete_data($table,$id)
		{
			$this->db->where("id",$id);
			return $this->db->delete($table);
		}
		function delete_login_data($user_type,$id)
		{
			$this->db->where("user_id",$id);
			$this->db->where("user_flag",$user_type);
			return $this->db->delete("login_detail");
		}
		function get_login_info($user_type,$id)
		{
			$this->db->select("email");
				$this->db->from("login_detail");
			$this->db->where("user_id",$id);
			$this->db->where("user_flag",$user_type);
			return $this->db->get()->row_array();
		}
		public function get_wallet_balance($uid)
		{
				$this->db->select("balance");
				$this->db->from('user_wallet');
				$this->db->where('user_id',$uid);
				$this->db->where('user_flag','Buyer');
				return $this->db->get()->row_array();
		}
		public function get_admin_wallet_balance()
		{
				$this->db->select("balance");
				$this->db->from('admin_wallet');
				$this->db->where('id',1);
				return $this->db->get()->row_array();
		}
		public function get_user_id($id, $flag)
		{
			$this->db->select('*');
			$this->db->from('login_detail');
			$this->db->where('user_id',$id);
			$this->db->where('user_flag',$flag);
			$result = $this->db->get();
			return (!empty($result->row())) ? $result->row() : false;
		}
	}