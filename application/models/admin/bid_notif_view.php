<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Scrap Interest
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Interest</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Buyer name</th>
										<th>Buyer Number</th>
										<th>Buyer address</th>
										<th>Scrap_id</th>
										<th>Message</th>
										<th>Status</th>
										
									</tr>
								</thead>
								<tbody>
							<?php if(!empty($scrap))
							{
								$i=1;
								foreach($scrap as $sc)
								{?>
									<tr id="currentdiv<?php echo $sc['interest_id'];?>">
									<td><?php echo $i;?></td>
									<td><?php if( $sc['name']!=''){echo $sc['name'];
									}?></td>	
									<td><?php if( $sc['phone_no']!=''){echo $sc['phone_no'];
									}?></td>
									<td><?php if( $sc['address']!=''){echo $sc['address'];
									}?></td>
									<td><?php if( $sc['order_id']!=''){ echo $sc['order_id'];}?></td>
									<td><?php if( $sc['name']!=''){ echo $sc['name']." is interested in ".$sc['order_id']." this scrap."; }?> </td>
									
									<td>
									<div id="before_status_change<?php echo $sc['interest_id'];?>">
										<?php if( $sc['status']==0){ ?>
										<span style="background-color:#05fc3e; cursor: pointer;"  class="label">new</span>
									<span style="background-color:#afed7d; cursor: pointer;"  class="label" onclick="markasview(<?php echo $sc['interest_id'];?>);">mark as viewed</span>
								<?php 
										}else{?><span style="background-color:#f9ef77; " class="label" >Viewed</span>
										<?php }?>
										</div>
										<div id="after_status_change<?php echo $sc['interest_id'];?>" style="display:none;">
										<span style="background-color:#f9ef77; " class="label" >Viewed</span>
										</div>
							</td>
									</tr>
							<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                  <!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <a href="javascript:void(0)" class="btn  btn-info btn-flat pull-left">Add New User</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
                  </div>
                  <!-- /.box-footer -->
                </div>
                <div class="clearfix"></div> 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();
	
});

var base_path='<?php echo base_url();?>';
 function markasview(id)
{
	var r = confirm("Are you sure you want to mark this interest as viewed?");
	if(r==true)
	{
	$.ajax({
			type : 'post',
			url : base_path+'admin/Scrap_controller/change_status',
			data : {
				id : id
			},
			success : function(data) {
		//	alert(data);
			   if(data==1)
			   {
					$("#before_status_change"+id).hide();
					$("#after_status_change"+id).show();
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}
</script>
</body>
</html>