<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_model extends CI_Model
{
	public function insert_query($table,$data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function select_query($table,$id)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id',$id);
		return $this->db->get()->row_array();
	}
	public function get_scrap_type()
	{
		$this->db->select('id,type');
		$this->db->from("scrap_type");
		$this->db->where('status',1);
		return $this->db->get()->result_array();
	}
	/* public function check_isMobileNoUniqe($phone)
	{
		$this->db->select('id');
		$this->db->from('user_data');
		$this->db->where('mobile',$phone);
		return $this->db->get()->row_array();
	} */
	public function check_isEmailUniqe($flag,$email)
	{
		$this->db->select('id,user_id');
		$this->db->from('login_detail');
		$this->db->where('email',$email);
		$this->db->where('user_flag',$flag);
		return $this->db->get()->row_array();
	}
	public function login($email,$pwd,$flag)
	{
		$this->db->select('id,user_id');
		$this->db->from('login_detail');
		$this->db->where('email',$email);
		$this->db->where('password',$pwd);
		$this->db->where('user_flag',$flag);
		return $this->db->get()->row_array();
	}
	public function update_query($table,$id,$token)
	{
		$this->db->where('id',$id);
		return $this->db->update($table,$token);
		//echo $this->db->last_query();die;
	}
	public function authentic_user($token,$flag)
	{
		$this->db->select('user_id as id');
		$this->db->from('login_detail');
		$this->db->where('token',$token);
	$this->db->where('user_flag',$flag);
		return $this->db->get()->row_array();
	}
	public function forgot_password($email,$flag)
	{
		$this->db->select('id,user_id');
		$this->db->from('login_detail');
		$this->db->where('email',$email);
		$this->db->where('user_flag',$flag);
		return $this->db->get()->row_array();
	}
	public function save_new_password($uid,$pass)
	{
		$this->db->where('id',$uid);
		return $this->db->update('login_detail',array('password'=>$pass));
	}
	public function get_scrap_data()
	{
	
		$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.desc,scrap_detail.quantity,scrap_detail.featured_image,scrap_detail.image1,scrap_detail.image2,scrap_detail.image3,scrap_detail.image4,scrap_detail.image5,scrap_detail.image6,scrap_detail.image7,scrap_detail.image8,scrap_detail.image9,scrap_detail.image10, scrap_detail.min_amt,scrap_type.type,scrap_detail.gst,scrap_detail.status');
		$this->db->from('scrap_detail');
		$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
		$this->db->where('scrap_detail.status !=',0);	
		$this->db->order_by('scrap_detail.status','ASC');
		$this->db->order_by('scrap_detail.id','DESC');
		return $this->db->get()->result_array();
		//echo $this->db->last_query();die;
	}
	public function check_isinterested($uid,$scrapid)
	{
		$this->db->select('id');
		$this->db->from('scrap_interest');
		$this->db->where('buyer_id',$uid);
		$this->db->where('scrap_id',$scrapid);
		return $this->db->get()->row_array();
	}
	public function check_is_bid_aded($uid,$scrapid)
	{
		$this->db->select('id,bid_amt');
		$this->db->from('scrap_bid');
		$this->db->where('buyer_id',$uid);
		$this->db->where('scrap_id',$scrapid);
		return $this->db->get()->row_array();
	}
	public function scrap_data_by_id($id)
	{
		$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.desc,scrap_detail.quantity,scrap_detail.image1,scrap_detail.image2,scrap_detail.image3,scrap_detail.image4,scrap_detail.min_amt,scrap_type.type,scrap_detail.gst');
		$this->db->from('scrap_detail');
		$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
		$this->db->where('scrap_detail.status',1);
		$this->db->where('scrap_detail.id',$id);
		return $this->db->get()->row_array();
	}
	public function check_is_old_pwd_correct($uid,$flag,$pwd)
	{
		$this->db->select('id');
		$this->db->from('login_detail');
		$this->db->where('user_id',$uid);
		$this->db->where('user_flag',$flag);
		$this->db->where('password',$pwd);
		return $this->db->get()->row_array();
	}
	public function get_buyer_deviceids()
	{
		$this->db->select('id,device_id');
		$this->db->from('login_detail');
		$this->db->where('user_flag','Buyer');
		$this->db->where('status',1);
		return $this->db->get()->result_array();
	}
	function get_seller_scrap_data($id)
	{
		$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.seller_id,scrap_detail.desc,scrap_detail.quantity,scrap_detail.image1,scrap_detail.image2,scrap_detail.image3,scrap_detail.image4,scrap_detail.status,scrap_type.type,scrap_detail.created_on as date');
		$this->db->from('scrap_detail');
			$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
		$this->db->where('seller_id',$id);
		$this->db->order_by('scrap_detail.id','DESC');
		return $this->db->get()->result_array();
	}
	function sold_scrap($scrap_id)
	{
		$this->db->select('scrap_payment_detail.scrap_id,buyer.name');
		$this->db->from('scrap_payment_detail');
		$this->db->join('buyer','scrap_payment_detail.buyer_id=buyer.id','left');
		$this->db->where('scrap_payment_detail.scrap_id',$scrap_id);
		return $this->db->get()->row_array();
	}
	function get_all_notifications($uid,$flag)
	{
		$this->db->select('id as notif_id,notification,created_on as date');
		$this->db->from('notifications');
		$this->db->where('user_id',$uid);
		$this->db->where('user_type',$flag);
		$this->db->order_by('id','DESC');
		return $this->db->get()->result_array();
	}
	function get_order_data($uid)
	{
		$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.desc,scrap_detail.quantity,scrap_detail.image1,scrap_detail.image2,scrap_detail.image3,scrap_detail.image4,scrap_detail.status,scrap_type.type,scrap_payment_detail.total_amt as amount,scrap_payment_detail.gst,scrap_payment_detail.sold_amt as total_amt,scrap_payment_detail.payed_amt,scrap_payment_detail.created_on as date');
		$this->db->from(' scrap_payment_detail');
		$this->db->join("scrap_detail","scrap_payment_detail.scrap_id=scrap_detail.id","left");
		$this->db->join("scrap_bid","scrap_payment_detail.scrap_id=scrap_bid.scrap_id","left");
		$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
		$this->db->where('scrap_payment_detail.buyer_id',$uid);
		$this->db->group_by('scrap_payment_detail.id');
		return $this->db->get()->result_array();
	}
	function get_bid_data($uid,$scrap_id)
	{
		$this->db->select('bid_amt');
		$this->db->from('scrap_bid');
		$this->db->where('scrap_id',$scrap_id);
		$this->db->where('buyer_id',$uid);
		return $this->db->get()->row_array();
	}
	function get_payment_data($uid)
	{
			$this->db->select('scrap_detail.id,scrap_detail.order_id,scrap_detail.quantity,scrap_payment_detail.sold_amt as total_amt,scrap_payment_detail.created_on as date');
			$this->db->from('scrap_payment_detail');
			$this->db->join("scrap_detail","scrap_payment_detail.scrap_id=scrap_detail.id","left");
			$this->db->join("scrap_bid","scrap_payment_detail.scrap_id=scrap_bid.scrap_id","left");
			$this->db->where('scrap_payment_detail.buyer_id',$uid);
			$this->db->group_by('scrap_payment_detail.id');
		return $this->db->get()->result_array();
	}
		function get_wallet_balance($uid,$flag)
		{
			$this->db->select('balance');
			$this->db->from('user_wallet');
			$this->db->where('user_id',$uid);
			$this->db->where('user_flag',$flag);
			return $this->db->get()->row_array();
		}
		function check_is_scrap_sold($s_id)
		{
			$this->db->select('id,sold_status');
			$this->db->from('scrap_bid');
			$this->db->where('scrap_id',$s_id);
			$this->db->where('sold_status !=',0);
			return $this->db->get()->row_array();
		}
		function get_data_for_invoice($uid,$scrap_id)
		{
			//echo $uid;echo $scrap_id;die;
			
			$this->db->select('scrap_payment_detail.id as invoice_id,scrap_payment_detail.gst,scrap_payment_detail.other_tax,scrap_payment_detail.sold_amt,scrap_payment_detail.total_amt,scrap_detail.quantity,scrap_detail.desc,scrap_type.type,buyer.name,buyer.phone_no,buyer.address,buyer.city,buyer.zip,scrap_detail.order_id,scrap_payment_detail.created_on as date');
			$this->db->from('scrap_payment_detail');
				$this->db->join("scrap_detail","scrap_payment_detail.scrap_id=scrap_detail.id","left");
				$this->db->join("scrap_type","scrap_detail.type_id=scrap_type.id","left");
				$this->db->join("buyer","scrap_payment_detail.buyer_id=buyer.id","left");
			$this->db->where('scrap_payment_detail.buyer_id',$uid);
			$this->db->where('scrap_payment_detail.scrap_id',$scrap_id);
			return $this->db->get()->row_array();
		}
		function update_wallet_balance($table,$uid,$flag,$data)
		{
			$this->db->select('id');
			$this->db->from($table);
			$this->db->where("user_id",$uid);
			$this->db->where("user_flag",$flag);
			$res=$this->db->get()->row_array();
			if(empty($res))
			{
				$this->db->insert($table,$data);
				return $this->db->insert_id();
			}else
			{
				$this->db->where("user_id",$uid);
				$this->db->where("user_flag",$flag);
				$this->db->set('balance', 'balance + ' . (int) $data["balance"], FALSE);
				return $this->db->update($table);
			}
		}
		function check_is_scrap_sold_by_me($uid,$scrap_id)
		{
			$this->db->select('scrap_payment_detail.scrap_id');
			$this->db->from('scrap_payment_detail');
			$this->db->where('scrap_id',$scrap_id);
			$this->db->where('buyer_id',$uid);
			return $this->db->get()->row_array();
		}
		public function get_user_data($uid,$flag)
	{
		if($flag=='Buyer')
		{
			$this->db->select('user_id,email,name');
			$this->db->from('login_detail');
			$this->db->join("buyer","login_detail.user_id=buyer.id","left");
			$this->db->where('login_detail.user_flag',$flag);
			$this->db->where('user_id',$uid);
			return $this->db->get()->row_array();
		}else
		{
			$this->db->select('user_id,email,seller.org_name as name');
			$this->db->from('login_detail');
			$this->db->join("seller","login_detail.user_id=seller.id","left");
			$this->db->where('login_detail.user_flag',$flag);
			$this->db->where('user_id',$uid);
			return $this->db->get()->row_array();
		}
		
	}
}