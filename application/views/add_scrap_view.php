<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Add New Scrap
        <!-- <small>Optional description</small> -->
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12">   
		   <?php if($this->session->flashdata('success')!=''){;?>
			<div class="alert alert-success no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold">Well done!</span>  <?php echo $this->session->flashdata('success');?>
			</div>
			 <?php } ?>
			 <?php if($this->session->flashdata('error')!=''){;?>
			<div class="alert alert-error no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold"</span>  <?php echo $this->session->flashdata('error');?>
			</div>
			 <?php } ?>
            <!-- form start -->
            <form role="form" name="scrap_add" id="scrap_add" method="POST" action="<?php echo base_url();?>admin/Scrap_controller/add_scrap_info" enctype="multipart/form-data" >  
              <div class="box box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">Scrap Info</h3>
                  </div>
                  <!-- /.box-header --> 
                    <div class="box-body">
					
                      <div class="form-group">
                        <label for="exampleInputEmail1">Scrap type</label>
						<select class="form-control" id="scrap_type" name="scrap_type">
						<option value="">Please select scrap type</option>
						<?php if(!empty($scrap_type))
						{
							foreach($scrap_type as $st)
							{?>
							
								<option value="<?php echo $st["id"];?>"><?php echo $st["type"];?></option>
						<?php }
						}?>
								</select>
                      </div> 
					    <div class="form-group">
                        <label for="exampleInputEmail1">Seller</label>
						<select class="form-control" id="seller_id" name="seller_id">
						<option value="">Please select Seller</option>
						<?php if(!empty($seller))
						{
							foreach($seller as $st)
							{?>
							
								<option value="<?php echo $st["id"];?>"><?php echo $st["org_name"];?></option>
						<?php }
						}?>
								</select>
                      </div> 
					  <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input class="form-control" id="desc" name="desc" placeholder="Enter decsription"  type="text">
                      </div> 
					  <div class="form-group qty-block">
                        <label for="exampleInputEmail1">Quantity</label>
                        <input class="form-control pull-left" id="qty" name="qty" placeholder="Enter Quantity" type="text">
					 <select class="form-control pull-left" id="unit" name="unit">
						
						<option value="kg">kg</option>
						<option value="ton">ton</option>
						<option value="ql">ql</option>
								</select>
								
					 </div>
                <style>
					.qty-block label {
						/* float: left; */
						width: 100%;
					}
					.qty-block .form-control {
						width: auto;
						margin-right: 5px;
					}
					.qty-block.has-error .help-block {
						color: #dd4b39;
						float: left;
						width: 100%;
					}
				</style>
					  
					  
                      <div class="form-group">
                        <label for="exampleInputFile"style="
    width: 100%;
    margin-top: 20px;
">Upload image</label>
                        <input id="image[]" name="image[]" type="file" multiple >
      
                      </div>
              
                    </div> 
                </div>
	
  <div class="form-group">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div> 
				  </div>
            </form>
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<?php $this->load->view("common/footer");?>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/custom_js/scrap.js"></script>

</body>
</html>