<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Seller List
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Seller</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Organization name</th>
										<th>Mobile No</th>
										<th>Address</th>
										<th>City</th>
										<th>Status</th>
										
									</tr>
								</thead>
								<tbody>
							<?php if(!empty($seller))
							{
								$i=1;
								foreach($seller as $sell)
								{?>
									<tr id="currentdiv<?php echo $sell['id'];?>">
									<td><?php echo $i;?></td>
									<td><?php if( $sell['org_name']!=''){echo $sell['org_name'];
									}?></td>
									<td><?php if( $sell['phone_no']!=''){ echo $sell['phone_no'];}?></td>
									<td><?php if( $sell['address']!=''){ echo $sell['address']; }?></td>
									<td><?php if( $sell['city']!=''){ echo $sell['city']; }?></td>
									
									<td>
									<a href="<?php echo base_url();?>admin/Admin_controller/edit_seller_view/<?php echo urlencode(base64_encode($sell['id']));?>"><span style="background-color:#EA4335; cursor: pointer;" class="label label-success">Edit</span></a>
									<a href="<?php echo base_url();?>admin/Admin_controller/view_seller_view/<?php echo urlencode(base64_encode($sell['id']));?>"><span style="background-color:#3C8DBC; cursor: pointer;" class="label">View</span></a>
									
							
									<span style="background-color:#fc0000; cursor: pointer;" class="label " onclick="delete_data(<?php echo $sell['id']; ?>);">Delete</span>

									<a href=# data-toggle="modal" data-target="#myModal"><span style="background-color:#4c504f; cursor: pointer;" class="label">Reset Password</span></a>
									<input type="hidden" id="seller_id" value="<?php if( $sell['id']!=''){ echo $sell['id']; }else{ echo "00";
									}?>">
								
							
							</td>
									</tr>
							<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  
    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reset Password</h4>
        </div>
        <div class="modal-body">
       		<label for="usr">Please enter new password:</label>
			<input type="text" class="form-control" id="new_password" name="new_password" required>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="save_password" class="btn btn-default">Save</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();

	$('#myModal').on('hidden.bs.modal', function () {
		$("#new_password").val('');
	})
	$('#save_password').click(function(){
     /* when the submit button in the modal is clicked, submit the form */
    var password = document.getElementById("new_password").value;
    var id = document.getElementById("seller_id").value;

    $.ajax({
			type : 'post',
			url : base_path+'admin/Admin_controller/update_user_password',
			data : {
				id : id,password:password,flag: 'Seller'
			},
			success : function(data) {
			//alert(data);
			   	if(data==1)
			   	{
				   alert("Password changed successfully");
				   location.reload();
				} else {
					alert("Something went wrong..please try later");
				}
			}
		});
	});
	
});

var base_path='<?php echo base_url();?>';
 function delete_data(id)
{
	var r = confirm("Are you sure you want to delete current record?");
	if(r==true)
	{
	$.ajax({
			type : 'post',
			url : base_path+'admin/Admin_controller/delete_seller_data',
			data : {
				id : id
			},
			success : function(data) {
		//	alert(data);
			   if(data==1)
			   {
				   alert("successfully deleted Record");
				   $("#currentdiv"+id).hide();
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}
</script>
</body>
</html>