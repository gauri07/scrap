<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Srap Admin | Log in</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
 
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <style type="text/css">
    .error{
      color:red;
    }
  </style>
  
  
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <!-- Extraordinary Life Style Admin -->
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!-- <p class="login-box-msg">Sign in to start your session</p> -->
    <form id="frm_register" name="frm_register" method="post" action="<?php echo base_url(); ?>admin/Admin_controller/change_password">

   <?php if($this->session->flashdata('success')!=''){;?>
			<div class="alert alert-success no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold">Well done!</span>  <?php echo $this->session->flashdata('success');?>
			</div>
			 <?php } ?>
			 <?php if($this->session->flashdata('error')!=''){;?>
			<div class="alert alert-error no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold"</span>  <?php echo $this->session->flashdata('error');?>
			</div>
			 <?php } ?>

     <h4>Please enter your registred email address.</h4>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 pull-right">
          <button type="submit" name="login" id="login" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

   
    <!-- /.social-auth-links -->

    <!-- <a href="#">I forgot my password</a><br> -->
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
