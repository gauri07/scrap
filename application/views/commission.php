<?php 
$CI =& get_instance();
$CI->load->model('administration_model');
$this->load->view("administration/includes/header");
$this->load->view('administration/includes/sidebar'); ?> 
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
.btn-logout{margin:10px;}
</style>
 <div class="wrapper">
  <div class="content-wrapper" style="min-height: 916px;">
    <section class="content-header">
      <h3>Commission List</h3>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <!--  <li><a href="#">Tables</a></li> -->
        <li class="active">Commission</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add</button>

            </div>

            <div id="all_message"></div>

              <!-- /.box-header -->
            <div class="box-body">
              <?php $msg=$this->session->flashdata('Success'); if(!empty($msg)){?>
              <div class="alert alert-success" id="successMessage" ><i class="fa fa-check-circle"></i><?php $msg=$this->session->flashdata('Success'); echo $msg; ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
            <?php }?>

              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12">
              <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Id</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Key</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Entry Value</th>
                     <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Commission Percentage(%)</th>
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Commission Amount</th>
                    
                    <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action</th>
                  </tr>
                </thead>
                <tbody>
                   <?php
                   $i=1;
                    foreach ($commissions as $key => $commission) { ?>
                     <tr role="row" class="odd">
                        <td class="sorting_1"><?php echo $i; ?></td>
                        <td><?php echo $commission['key_name']; ?></td>
                        <td><?php echo $commission['entry_value']; ?></td>
                        <td><?php echo $commission['com_percentage']; ?></td>
                        <td><?php echo $commission['com_amount']; ?></td>
                        <td><a data-toggle="modal" data-target="#myModalEdit" title="Edit" alt="Edit" id="edit"  class="editPayment" data-option="<?php echo $commission['id']; ?>" href="javascript:void(0)"><i class="fa fa-edit"></i></a> |
                        <a alt="Delete" id="delete"  class="deleteRecord" data-option="<?php echo $commission['id']; ?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                        </td>
                        <!-- <td><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add</button></td> -->


                     </tr> 
                   <?php $i++;} ?> 
                </tbody>
                <tfoot>  
                </tfoot>
              </table></div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
</div>


<!--Add Commission Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <!--  <button type="button" class="close mfp-close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Add Commission</h4>
      </div>
      <div class="modal-body">
      <div id="message"></div>
      	<form  class="form-horizontal" method="post" action="" id="formUser" name="formUser">
      	<p>Key Name</p>
        <input type="text" name="txt_key_name" id="txt_key_name" value="">
        <span id="keyname" class="error"></span> 
        <br>
        <p>Entry Value.</p>
        <input type="text" name="txt_entry_value" id="txt_entry_value" value="">
        <span id="entryvalue" class="error"></span> 
        <br>
        <p>Commission Percentage(%).</p>
        <input type="text" name="txt_com_percentage" id="txt_com_percentage" value="">
        <span id="percentageInfo" class="error"></span> 
      	<br>
      	<p>Commission Amount.</p>
        <input type="text" name="txt_com_amount" id="txt_com_amount" value="">
        <span id="comAmt" class="error"></span> 
        <br>
      </div>
     
      <div class="modal-footer">
       <button type="button" id="add_close" class="btn btn-default mfp-close btn_close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn button btn-default mfp-close" name="btn_save" id="btn_save">Submit</button>
       
      </div>
       </form>
    </div>  
  </div>
</div>
<!--end Add Commission Modal -->

<!--Edit Commission Modal -->
<div id="myModalEdit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Edit Commission</h4>
      </div>
      <div class="modal-body">
       <div id="edit_message"></div>
      <div id="message"></div>
      	<form  class="form-horizontal" method="post" action="" id="formEditUser" name="formEditUser">
      	<p>Key Name</p>
        <input type="text" name="txt_key_name1" id="txt_key_name1" value="">
        <span id="keyname1" class="error"></span> 
        <br>
        <p>Entry Value.</p>
        <input type="text" name="txt_entry_value1" id="txt_entry_value1" value="">
        <span id="entryvalue1" class="error"></span> 
        <br>
        <p>Commission Percentage(%).</p>
        <input type="text" name="txt_com_percentage1" id="txt_com_percentage1" value="">
        <span id="percentageInfo1" class="error"></span> 
      	<br>
      	<p>Commission Amount.</p>
        <input type="text" name="txt_com_amount1" id="txt_com_amount1" value="">
        <span id="comAmt1" class="error"></span> 
        <br>
      </div>
     
      <div class="modal-footer">
      	<input type="hidden" name="id" id="id" value="" class="gui-input">
      	<button type="button" class="btn btn-default mfp-close btn_close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn button btn-default mfp-close" name="btn_edit_save" id="btn_edit_save">Submit</button>
      </div>
       </form>
    </div>  
  </div>
</div>
<!--end Edit Commission Modal -->



<style>
.event-children {
    margin-left: 20px;
    list-style: none;
    display: none;
}
#vehicle-categories{position:absolute;}
.event-children {
    margin: 0 0 0 13px !important;
    padding: 3px 0;
}
 .pm {
    border: 1px solid #CCCCCC;
    padding: 5px;
}.set_category {
    background-color: #E4E4E4;
    height: 300px;
    left: -1px !important;
    list-style: none outside none;
    overflow-y: auto;
    padding: 15px;
    top: 28px;
    width: 315px !important;
    z-index: 99999 !important;
}
.thisshow{ display:none;} 
.thisshow1{ display:none;} 
</style>

<script> 

$('#txt_com_percentage').keyup(function () { 
	var val= $("#txt_entry_value").val();
	if(val ==''){
		$("#entryvalue").html("Plase enter entry value");
		$flag=0;
	}
	this.value = this.value.replace(/[^0-9\.]/g,''); 
});
$("#txt_entry_value").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 
function delete_confirm()
{
	var record_delete =confirm("Are you sure to delete this Record?");
	return record_delete;
}

$(document).on("click",".btn_close",function() {
	$("#id").val("");
	$("#txt_key_name").html("");
	$("#txt_entry_value").html("");
	$("#txt_com_percentage").html("");  	
	$("#txt_com_amount").html(""); 
	document.getElementById("formUser").reset();
	location.reload();
});

function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	//$("#message div").fadeOut(10000);
	if(json.action == 'edit')
	{	
		$("#edit_message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	}
	else if(json.action == 'delete'){
		$("#all_message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	}
	if(json.status == 1)
	{	
		setTimeout(function(){ location.reload(); }, 3000);
	}
}

$(document).on("click","#delete",function(){
	var id = $(this).attr('data-option'); 
	if(id!=''){
		$.ajax({
			type:'POST',
			dataType:"json",
			url:"<?php echo base_url(); ?>administration/delete",
			data: {"id":id},
		}).success(function(json){
			showMessages(json);
		});
	}
});


$(document).on("click","#edit",function() { 
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//return false;
	$("#id").val(id);
	if(id!=""){
		$.ajax({
			type: "POST",
			dataType: "json",
			//fileElementId :'user_photo',
			url: "<?php echo base_url(); ?>administration/getCommissionById",
				data: {"id":id},
			}).success(function (json) {
				$("#txt_key_name1").val(json.key_name);
				$("#txt_entry_value1").val(json.entry_value);
				$("#txt_key_name1").attr('readonly','readonly');
				//$("#txt_entry_value").attr('readonly','readonly');
				$("#txt_com_percentage1").val(json.com_percentage);
				$("#txt_com_amount1").val(json.com_amount);
			}); 
	} 
});
// $(document).on("click","#myModel",function() {
// 	$("#id").val("");
// 	$("#txt_key_name").html("");
// 	$("#txt_entry_value").html("");  	
// 	$("#txt_com_percentage").html("");  	
// 	$("#txt_com_amount").html("");  
// 	document.getElementById("formUser").reset();
// 	location.reload();
// });

function refreshTable()
{
	$("#txt_search").val("");
	$("#dd_searchBy1").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	
}
// add commission
$(document).ready(function(){  
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var id = $("#id"); 
		var keyName = $("#txt_key_name");
			var keynameInfo = $("#keyname");  	
		var entryValue = $("#txt_entry_value");
			var entryvalueInfo = $("#entryvalue");  	
		var percentage = $("#txt_com_percentage");
			var perInfo = $("#percentageInfo"); 
		var comAmount = $("#txt_com_amount");
			var comAmtInfo = $("#comAmt");				
		var flag=1;
		if(!validateEmpty(keyName, keynameInfo, "key name")){
			flag = 0;
		} 	
		if(!validateEmpty(entryValue, entryvalueInfo, "entry Value")){
			flag = 0;
		}
		if(!validateEmpty(percentage, perInfo, "percentage")){
			flag = 0;
		} 
		if(!validateEmpty(comAmount, comAmtInfo, "amount")){
			flag = 0;
		} 
		$("#txt_com_percentage").keyup(function(){
			var entry_value= $("#txt_entry_value").val();
			var percentage_value= $("#txt_com_percentage").val();
			var comm_amt= $("#txt_com_amount").val();

			if(entry_value!=''&& percentage_value!=''){
				var amt= entry_value*(percentage_value/100);
				$("#txt_com_amount").val(amt);
			}
		}).keydown(function( event ) {
		  if ( event.which == 13 ) {
		    event.preventDefault();
		  }
	    });  

		$("#txt_com_amount").keyup(function(){
			var entry_value= $("#txt_entry_value").val();
			var comm_amt= $("#txt_com_amount").val();
			if(entry_value!=''&& comm_amt!=''){
				var per= (comm_amt/entry_value)*100;
				$("#txt_com_percentage").val(per);
			}
		}).keydown(function( event ) {
		  if ( event.which == 13 ) {
		    event.preventDefault();
		  }
		});

		var formData = new FormData($('#formUser')[0]); 
		if(flag)
		{
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",	
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_Commission",
				data: formData
			}).success(function (json) {
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				
			});
		} 
	});		
});

//edit commission

$(document).ready(function(){  
	$('#btn_edit_save').on('click', function (e) {
		
		e.preventDefault();
		var id = $("#id"); 
		var keyName = $("#txt_key_name1");
			var keynameInfo = $("#keyname1");  	
		var entryValue = $("#txt_entry_value1");
			var entryvalueInfo = $("#entryvalue1");  	
		var percentage = $("#txt_com_percentage1");
			var perInfo = $("#percentageInfo1"); 
		var comAmount = $("#txt_com_amount1");
			var comAmtInfo = $("#comAmt1");				
		var flag=1;
		if(!validateEmpty(keyName, keynameInfo, "key name")){
			flag = 0;
		} 
		if(!CheckNumerals(entryValue, entryvalueInfo, "entry Value")){
			flag = 0;
		}   
		// else if(entryValue.val()!=''){

			
		// }	
		if(!validateEmpty(percentage, perInfo, "percentage")){
			flag = 0;
		} 
		if(!validateEmpty(comAmount, comAmtInfo, "amount")){
			flag = 0;
		} 
		$("#txt_com_percentage1").keyup(function(){
			console.log('in edit');
			var entry_value= $("#txt_entry_value1").val();
			var percentage_value= $("#txt_com_percentage1").val();
			var comm_amt= $("#txt_com_amount1").val();

			if(entry_value!=''&& percentage_value!=''){
				var amt= entry_value*(percentage_value/100);
				$("#txt_com_amount1").val(amt);
			}
		}).keydown(function( event ) {
		  if ( event.which == 13 ) {
		    event.preventDefault();
		  }
	    });  

		$("#txt_com_amount1").keyup(function(){
			var entry_value= $("#txt_entry_value1").val();
			var comm_amt= $("#txt_com_amount1").val();
			if(entry_value!=''&& comm_amt!=''){
				var per= (comm_amt/entry_value)*100;
				$("#txt_com_percentage1").val(per);
			}
		}).keydown(function( event ) {
		  if ( event.which == 13 ) {
		    event.preventDefault();
		  }
		});

		var formData = new FormData($('#formEditUser')[0]); 
		if(flag)
		{
			$("#btn_edit_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",	
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>administration/save_Commission",
				data: formData
			}).success(function (json) {
				showMessages(json);
				$("#btn_edit_save").removeAttr('disabled','disabled');
				//$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
			});
		}
	});
});




function deleteCommission()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected Commission?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>administration/delete_Commission",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				alert((json.ids).length+" user(s) has been deleted.");
				location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("administration/includes/footer"); ?>
