<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html');
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ezyscrap</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  
<link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.tableTools.min.css" rel="stylesheet" type="text/css">
	 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/form_validation/formValidation.css"/> 
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Ezyscrap
            <small class="pull-right">Date: <?php echo date("d/m/Y");?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
            <address>
            <strong>Bitware technologies</strong><br>
            601, 6th Floor,<br>
                Wakad Road,White Square Building, <br>
                Hinjewadi, Pune, Maharashtra 411057 <br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php if($data['name']!=''){echo $data['name'];}?></strong><br>
          <?php if($data['address']!=''){echo $data['address'];}?><br>
           <?php if($data['city']!=''){echo $data['city'];}?>, <?php if($data['zip']!=''){echo $data['zip'];}?><br>
            Phone:<?php if($data['zip']!=''){echo $data['zip'];}?><br>
            Email: <?php if($data['name']!=''){echo $data['name'];}?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #00<?php if($data['invoice_id']!=''){echo $data['invoice_id'];}?></b><br>
          <br>
          <b>Order ID:</b> <?php if($data['order_id']!=''){echo $data['order_id'];}?><br>
          <b>Payment Date:</b> <?php if($data['date']!=''){echo date("d/m/Y",strtotime($data['date'])) ;}?><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Scrap type</th>
              <th>Description</th>
              <th>Qty </th>
              <th>Rate/Price</th>
              <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
               <tr>
            <td><?php if($data['type']!=''){echo $data['type'];}?></td>
            <td><a href=\"#\"><?php if($data['desc']!=''){echo $data['desc'];}?></a></td>
            <td class=\"text-right\"><?php if($data['quantity']!=''){echo $data['quantity'];}?></td>
            <td class=\"text-right\"><?php if($data['total_amt']!=''){echo $data['total_amt'];}?></td>
            <td class=\"text-right\"><?php if($data['total_amt']!=''){echo $data['total_amt'];}?></td>
          </tr>
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->

        <!-- /.col -->
        <div class="col-xs-6 col-md-offset-6">
      

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php if($data['total_amt']!=''){echo $data['total_amt'];}?></td>
              </tr>
              <tr>
                <th>GST</th>
                <td><?php if($data['gst']!=''){echo $data['gst'];}?></td>
              </tr>
         
              <tr>
                <th>Total:</th>
                <td><?php if($data['sold_amt']!=''){echo $data['sold_amt'];}?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
     
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
 

  

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
