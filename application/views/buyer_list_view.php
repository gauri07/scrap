<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Buyer List
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Buyer</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="buyer-list table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Name</th>
										<th>Mobile No</th>
										<th>Address</th>
										<th>Licence</th>
										<th>Wallet Balance</th>
										<th>Registation Fees</th>
										<th>Status</th>
										
									</tr>
								</thead>
								<tbody>
							<?php if(!empty($buyer))
							{
								$i=1;
								foreach($buyer as $buy)
								{?>
									<tr id="currentdiv<?php echo $buy['id'];?>">
									<td><?php echo $i;?></td>
									<td><?php if( $buy['name']!=''){echo $buy['name'];
									}?></td>
									<td><?php if( $buy['phone_no']!=''){ echo $buy['phone_no'];}?></td>
									<td><?php if( $buy['address']!=''){ echo $buy['address']; }?></td>
									<td> <img src="<?php if( $buy['licence']!=''){ echo $buy['licence']; }?>"  class="img-thumbnail img-width" ></td>
									<td><?php if( $buy['balance']!=''){ echo round($buy['balance'],2); }?></td>
									
									<td>
									<div id="before_status_change<?php echo isset($sc['id']) ? $sc['id'] : '' ;?>">
									<?php if( $buy['reg_fee']==1){?><span style="background-color:#D74300; cursor: pointer;" class="label label-success">paid</span><?php } elseif( $buy['reg_fee']==0) {?><span style="background-color:#f39c12; " class="label" >Pending</span><?php } ?>
									</div>
									</td>
									<td>
									<a href="<?php echo base_url();?>admin/Admin_controller/edit_buyer_view/<?php echo urlencode(base64_encode($buy['id']));?>"><span style="background-color:#EA4335; cursor: pointer;" class="label label-success">Edit</span></a>	
									<a href="<?php echo base_url();?>admin/Admin_controller/view_buyer_view/<?php echo urlencode(base64_encode($buy['id']));?>"><span style="background-color:#3C8DBC; cursor: pointer;" class="label">View</span></a>
									<input type="hidden" id="imagename<?php echo $buy['id']; ?>" value="<?php if( $buy['licence']!=''){ echo $buy['licence']; }else{ echo "00";
									}?>">
							
									<span style="background-color:#fc0000; cursor: pointer;" class="label " onclick="delete_data(<?php echo $buy['id']; ?>);">Delete</span>

									<a href=# data-toggle="modal" data-target="#myModal"><span style="background-color:#4c504f; cursor: pointer;" class="label">Reset Password</span></a>
									<input type="hidden" id="buyer_id" value="<?php if( $buy['id']!=''){ echo $buy['id']; }else{ echo "00";
									}?>">
								
							
							</td>
									</tr>
							<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                  <!-- /.box-body -->
                 
                </div>
                <div class="clearfix"></div> 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reset Password</h4>
        </div>
        <div class="modal-body">
       		<label for="usr">Please enter new password:</label>
			<input type="text" class="form-control" id="new_password" name="new_password" required>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="save_password" class="btn btn-default">Save</button>
        </div>
      </div>
      
    </div>
  </div>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();

	$('#save_password').click(function(){
     /* when the submit button in the modal is clicked, submit the form */
    var password = document.getElementById("new_password").value;
    var id = document.getElementById("buyer_id").value;

    $.ajax({
			type : 'post',
			url : base_path+'admin/Admin_controller/update_user_password',
			data : {
				id : id,password:password, flag: 'Buyer'
			},
			success : function(data) {
			//alert(data);
			   	if(data==1)
			   	{
				   alert("Password changed successfully");
				   location.reload();
				} else {
					alert("Something went wrong..please try later");
				}
			}
		});
	});
	
});

var base_path='<?php echo base_url();?>';
 function delete_data(id)
{
	var r = confirm("Are you sure you want to delete current record?");
	if(r==true)
	{
		var img=$("#imagename"+id).val();
	$.ajax({
			type : 'post',
			url : base_path+'admin/Admin_controller/delete_buyer_data',
			data : {
				id : id,img:img
			},
			success : function(data) {
			console.log(data);
			   if(data==1)
			   {
				   alert("successfully deleted Record");
				   $("#currentdiv"+id).hide();
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}
</script>
</body>
</html>