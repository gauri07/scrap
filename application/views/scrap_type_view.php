<?php $this->load->view("common/header");?>
<style>
.img-width {
    width: 140px;
    height: auto;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Scrap Type List
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Scrap Type</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
    <!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <a class="btn  btn-info btn-flat pull-left" data-toggle="modal" href="#addModel">Add New Scrap type</a>
                    <!-- <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a> -->
                  </div>
                  <!-- /.box-footer -->
                </div>
                <div class="clearfix"></div> 
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Type</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($scrap_type))
							{
								$i=1;
								foreach($scrap_type as $sc)
								{?>
									<tr id="currentdiv<?php echo $sc['id'];?>">
									<td><?php echo $i;?></td>
									<td id="type_td<?php echo $sc['id'];?>"><?php if( $sc['type']!=''){ echo $sc['type'];}?></td>
									<td><?php if($sc['status']==1){?><span style="background-color:#EA4335; cursor: pointer;" class="label label-success" onclick="change_status(<?php echo $sc['id'];?>,0,this);">ACTIVE</span><?php }else {?><span style="background-color:#DD4B39; cursor: pointer;" class="label label-danger" onclick="change_status(<?php echo $sc['id'];?>,1,this);">DEACTIVE</span><?php 
							}?></td>
									<td>
									
											<span style="background-color:#EA4335; cursor: pointer;" class="label label-success" data-toggle="modal" href="#editModel<?php echo $sc['id'];?>">Edit</span>
											
									
							
									<span style="background-color:#fc0000; cursor: pointer;" class="label " onclick="delete_data(<?php echo $sc['id']; ?>);">Delete</span>
									</td>
									</tr>
									<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                
                </div>
                <div class="clearfix"></div> 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <?php if(!empty($scrap_type))
							{
							
								foreach($scrap_type as $sc)
								{?>
  <div id="editModel<?php echo $sc['id'];?>" class="modal fade" role="dialog">
								  <div class="modal-dialog">
  <div class="control-sidebar-bg"></div>
	<div class="modal-content">
									  <div class="modal-header">
										<a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="<?php echo base_url();?>images/logo-wide.png" alt=""></a>
										<button type="button" class="close" data-dismiss="modal">&times;</button>			
									  </div>
									  
									  <div class="modal-body" id="disputebody">
										<div class="tab-content">
										  <div id="login" class="tab-pane fade in active">
											<form name="login-form" class="clearfix">
												
											  <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Type</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input type="text"  id="type<?php echo $sc['id'];?>" value="<?php echo $sc['type'];?>" name="type<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
												
												 
											  <div class="form-group mt-10">
											<div class="col-md-4">
												<button type="button" onclick="edit_type(<?php echo $sc['id'];?>);" class="btn btn-default btn-flat btn-theme-colored" >Submit</button>
											  </div>
											  <div class="clear text-center pt-10">
												
											  </div>
											</form>
										  </div>
										  
										</div>
									  </div>
									  
									</div>
									</div>
									</div>
									</div>
								<?php 
						
								}
							}?>
							
							<div id="addModel" class="modal fade" role="dialog">
								  <div class="modal-dialog">
  <div class="control-sidebar-bg"></div>
	<div class="modal-content">
									  <div class="modal-header">
										<a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="<?php echo base_url();?>images/logo-wide.png" alt=""></a>
										<button type="button" class="close" data-dismiss="modal">&times;</button>			
									  </div>
									  
									  <div class="modal-body" id="disputebody">
										<div class="tab-content">
										  <div id="login" class="tab-pane fade in active">
											<form name="login-form" class="clearfix">
												
											  <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Type</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input type="text"  id="type" placeholder="Please enter scrap type" name="type" >
												  </div>
												  </div>
												</div>
												
												 
											  <div class="form-group mt-10">
											<div class="col-md-4">
												<button type="button" onclick="add_type();" class="btn btn-default btn-flat btn-theme-colored" >Submit</button>
											  </div>
											  <div class="clear text-center pt-10">
												
											  </div>
											</form>
										  </div>
										  
										</div>
									  </div>
									  
									</div>
									</div>
									</div>
									</div>
<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();
	
});

var base_path='<?php echo base_url();?>';

function edit_type(type_id)
{
	var type=$("#type"+type_id).val();
		if(type=='')
		{
			alert("please enter Scrap type");
		}
		else{
	 var t=confirm("Are you sure you want to edit this scrap type?");
		 if(t==true)
		 {
					$.ajax({
					type : 'post',
					url : base_path+'admin/Scrap_controller/edit_scrap_type',
					data : {
						type_id:type_id,type:type
					},
					success : function(data) {
						//alert(data);
					if(data==1){
							$("#type_td"+type_id).html(type);
						   	$("#editModel"+type_id).modal("hide");
						//	window.location =base_path+'admin/Scrap_controller';
						
					} else {
						alert("Somthing went wrong....! Please try after some time. Thank you.");
					}
		 }
	});
}
		 }
}
function add_type()
{
	var type=$("#type").val();
	var type_id=0;
		if(type=='')
		{
			alert("please enter Scrap type");
		}
		else{
	 var t=confirm("Are you sure you want to Add this scrap type?");
		 if(t==true)
		 {
					$.ajax({
					type : 'post',
					url : base_path+'admin/Scrap_controller/edit_scrap_type',
					data : {
						type_id:type_id,type:type
					},
					success : function(data) {
						//alert(data);
					if(data==1){
						alert("successfully added new scrap type");
						   	$("#addModel").modal("hide");
							window.location =base_path+'admin/Scrap_controller/scrap_type_view';
						
					} else {
						alert("Somthing went wrong....! Please try after some time. Thank you.");
					}
		 }
	});
}
		 }
	}


function delete_data(id)
{
	
	var r = confirm("Are you sure you want to delete current record?");
	if(r==true)
	{
	$.ajax({
			type : 'post',
			url : base_path+'admin/Scrap_controller/delete_data',
			data : {
				id : id
			},
			success : function(data) {
			//alert(data);
			//alert(data);
			   if(data==1)
			   {
				   alert("successfully deleted Record");
			
				   $("#currentdiv"+id).hide();
				   $("#minBidAmt"+id).html();
				   
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}
 function change_status(id,status1,element)
{
	//alert("ffd");
	//var base_path=<?php echo base_url();?>;
	
	var r = confirm("Are you sure you want to change the status of current record?");
	if(r==true)
	{
		var data={
			"cid":id,
			"status":status1
		}
		 $.ajax({
                type : 'post',
                url : base_path+'admin/Scrap_controller/change_type_status',
                data : {
                    id : id, status1 : status1
                },
                success : function(data) {
                   // alert(data);
           
				if(data==1)
				{
					if(status1==0)
					{
						$(element).html('DEACTIVE');
						$(element).removeClass('label-success');
						$(element).addClass('label-danger');
						$(element).attr('onclick',"change_status("+id+",1,this)");
					}
					else if(status1==1)
					{
						$(element).html('ACTIVE');
						$(element).removeClass('label-danger');
						$(element).addClass('label-success');
						$(element).attr('onclick',"change_status("+id+",0,this)");
					}
					$('#msg').text( "Scrap type's status changed successfully").css('color','#20df38');
				}else{
				$('#msg').text( "Fail to change status, Please try again.").css('color','#ff0000');
				}  
			}
		});
	}
}


</script>
</body>
</html>