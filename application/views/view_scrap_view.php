<style type="text/css">
  label > input{ /* HIDE RADIO */
  visibility: hidden; /* Makes input not-clickable */
  position: absolute; /* Remove input from document flow */
}
label > input + img{ /* IMAGE STYLES */
  cursor:pointer;
  border:2px solid transparent;
}
label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
  border:2px solid #f00;
}
</style>
<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Scrap info
        <!-- <small>Optional description</small> -->
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12">   
		
            <!-- form start -->
            <form role="form" name="scrap_edit" id="scrap_edit" method="POST" action="<?php echo base_url();?>admin/Scrap_controller/edit_scrap_info" enctype="multipart/form-data" >  
              <div class="box box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">Scrap Info</h3>
                  </div>
                  <!-- /.box-header --> 
                    <div class="box-body">
					<div class="form-group">
					 <input class="form-control" id="id" name="id" value="<?php if(isset($scrap["id"])){ echo $scrap["id"];}?>" placeholder="Enter Name"  type="hidden">
					
					 </div>
                            <div class="form-group">
                        <label for="exampleInputEmail1">Scrap type</label>
						<input class="form-control" id="desc" name="desc" value="<?php if(isset($scrap["type"])){ echo $scrap["type"];}?>" type="text" readonly>
                      </div> 
					    <div class="form-group">
                        <label for="exampleInputEmail1">Seller</label>
							<input class="form-control" id="desc" name="desc" value="<?php if(isset($scrap["org_name"])){ echo $scrap["org_name"];}?>" type="text" readonly>
                      </div> 
                    
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <input class="form-control" id="desc" name="desc" value="<?php if(isset($scrap["desc"])){ echo $scrap["desc"];}?>"  type="text" readonly>
                      </div>
					    <div class="form-group">
                        <label for="exampleInputEmail1">Quantity</label>
                        <input class="form-control" id="quantity" name="quantity" placeholder="Enter Quantity"  value="<?php if(isset($scrap["quantity"])){ echo $scrap["quantity"];}?>" type="text" readonly>
                     <div class="form-group">
                        <label for="exampleInputEmail1">GST %</label>
                        <input class="form-control" id="gst" name="gst" placeholder="Enter gst %"  value="<?php if(isset($scrap["gst"])){ echo $scrap["gst"];}?>" type="text" readonly>
                       </div>
					    <div class="form-group">
                        <label for="exampleInputEmail1">Seller amount %</label>
                        <input class="form-control" id="seller_amt" name="seller_amt" placeholder="Enter seller amount %"  value="<?php if(isset($scrap["seller_amt_per"])){ echo $scrap["seller_amt_per"];}?>" type="text" readonly>
                       </div>
					   <div class="form-group">
                        <label for="exampleInputEmail1">Minimum Bid amount </label>
                        <input class="form-control" id="bid_amt" name="bid_amt" placeholder="Enter seller amount %"  value="<?php if(isset($scrap["min_amt"])){ echo $scrap["min_amt"];}?>" type="text" readonly>
                       </div>

                      <div class="row">
                      <label style="margin-left: 18px;">Selected Image will be set as featured image.</label>
                      <div class="col-md-12">
					  <?php if( $scrap['image1']!=''){?>
                      <div class="col-md-3">
                        <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image1']!='' ? $scrap['image1'] : null ?>")' <?php if(isset ($scrap['image1']) && $scrap['image1'] == $scrap['featured_image']) { ?> checked <?php } ?>/>
					                 <img  id="scrapimage" src="<?php if( $scrap['image1']!=''){ echo $scrap['image1']; }?>"  class="img-thumbnail img-width" >
                        </label>
                      </div>
					<?php }?>
					<?php if( $scrap['image2']!=''){ ?>					  
                  <div class="col-md-3">
                    <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image2']!='' ? $scrap['image2'] : null ?>")' <?php if(isset ($scrap['image2']) && $scrap['image2'] == $scrap['featured_image']) { ?> checked <?php } ?>/>
                      <img  id="scrapimage" src="<?php if( $scrap['image2']!=''){ echo $scrap['image2']; }?>"  class="img-thumbnail img-width" >
                    </label>
                  </div>
					<?php }if( $scrap['image3']!=''){?>
                  <div class="col-md-3">
                    <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image3']!='' ? $scrap['image3'] : null ?>")' <?php if(isset ($scrap['image3']) && $scrap['image3'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                      <img  id="scrapimage" src="<?php if( $scrap['image3']!=''){ echo $scrap['image3']; }?>"  class="img-thumbnail img-width" >
                    </label>
                  </div>
					<?php }?>
					<?php if( $scrap['image4']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image4']!='' ? $scrap['image4'] : null ?>")' <?php if(isset ($scrap['image4']) && $scrap['image4'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image4']!=''){ echo $scrap['image4']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
					<?php } ?>
          <?php if( $scrap['image5']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image5']!='' ? $scrap['image5'] : null ?>")' <?php if(isset ($scrap['image5']) && $scrap['image5'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image5']!=''){ echo $scrap['image5']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
          <?php if( $scrap['image6']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image6']!='' ? $scrap['image6'] : null ?>")' <?php if(isset ($scrap['image6']) && $scrap['image6'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image6']!=''){ echo $scrap['image6']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
          <?php if( $scrap['image7']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image7']!='' ? $scrap['image7'] : null ?>")' <?php if(isset ($scrap['image7']) && $scrap['image7'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image7']!=''){ echo $scrap['image7']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
          <?php if( $scrap['image8']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image8']!='' ? $scrap['image8'] : null ?>")' <?php if(isset ($scrap['image8']) && $scrap['image8'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image8']!=''){ echo $scrap['image8']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
          <?php if( $scrap['image9']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image9']!='' ? $scrap['image9'] : null ?>")' <?php if(isset ($scrap['image9']) && $scrap['image9'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image9']!=''){ echo $scrap['image9']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
          <?php if( $scrap['image10']!=''){?>
                <div class="col-md-3">
                  <label><input type="radio" name="fb" value="small" onclick='image_select("<?php echo $scrap['image10']!='' ? $scrap['image10'] : null ?>")' <?php if(isset ($scrap['image10']) && $scrap['image10'] == $scrap['featured_image']) { ?> checked <?php } ?> />
                    <img  id="scrapimage" src="<?php if( $scrap['image10']!=''){ echo $scrap['image10']; }?>"  class="img-thumbnail img-width" >
                  </label>
                </div>
          <?php } ?>
      
                      </div>
                      </div>
              
                    </div> 
                    </div> 
              
	
 
            </form>
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<?php $this->load->view("common/footer");?>

 
</body>

</html>
<script type="text/javascript">
  var base_path='<?php echo base_url();?>';
  function image_select(image)
  {
    // alert(image);
    var id = <?php echo $scrap_id; ?>;
    // alert(id);
    $.ajax({
      type : 'post',
      url : base_path+'admin/Scrap_controller/set_featured_image',
      data : {
        featured_image : image, id: id
      },
      success : function(data) {
      // alert(data);
          if(data==1)
          {
           alert("Featured image set successfully");
           // location.reload();
        } else {
          alert("Something went wrong..please try later");
        }
      }
    });
  }
</script>