<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Bid
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Bid</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Buyer name</th>
										<th>Buyer Number</th>
										<th>Buyer address</th>
										<th>Scrap_id</th>
										<th>Bid Amount</th>
										<th>Message</th>
										<th>Status</th>
										<!--<th>Status</th>-->
										
									</tr>
								</thead>
								<tbody>
							<?php if(!empty($scrap))
							{
								$i=1;
								foreach($scrap as $sc)
								{?>
									<tr id="currentdiv<?php echo $sc['bid_id'];?>">
									<td><?php echo $i;?></td>
									<td><?php if( $sc['name']!=''){echo $sc['name'];
									}?></td>	
									<td><?php if( $sc['phone_no']!=''){echo $sc['phone_no'];
									}?></td>
									<td><?php if( $sc['address']!=''){echo $sc['address'];
									}?></td>
									<td><?php if( $sc['order_id']!=''){ echo $sc['order_id'];}?></td>
									<td><?php if( $sc['bid_amt']!=''){ echo $sc['bid_amt'];}?></td>
									<td><?php if( $sc['name']!=''){ echo $sc['name']." has placed bid.Bid amount is".$sc['bid_amt'] ;}?> </td>
									<td>
								<!--	<div id="before_status_change<?php echo $sc['bid_id'];?>">
										<?php if( $sc['status']==0){ ?>
										<span style="background-color:#05fc3e; cursor: pointer;"  class="label">new</span>
									<span style="background-color:#afed7d; cursor: pointer;"  class="label" onclick="markasview(<?php echo $sc['bid_id'];?>);">mark as viewed</span>
								<?php 
										}else{?><span style="background-color:#00ffed; " class="label" >Viewed</span>
										<?php }?>
										</div>-->
										<!--<div id="after_status_change<?php echo $sc['bid_id'];?>" style="display:none;">
										<span style="background-color:#00ffed; " class="label" >Viewed</span>
										</div>-->
										<?php if( $sc['sold_status']==0){ ?>
										<span style="background-color:#4be59b;cursor: pointer;" id="approve_span" class="label" onclick="approve_bid(<?php echo $sc['bid_id'];?>,<?php echo $sc['scrap_id'];?>);" >Approve bid</span>
										<?php }
										elseif( $sc['sold_status']==1){ 
										?>
										<span style="background-color:#f39c12" class="label"   id="approved_span">approved</span>
										<?php }
										elseif( $sc['sold_status']==2){ 
										?>
										<span style="background-color:#f39c12; " class="label"  id="sold_out">Sold out</span>
										<?php }
										?>
										
							</td>
								
									</tr>
							<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();
	
});

var base_path='<?php echo base_url();?>';
function approve_bid(bid_id,scrap_id)
{
	
	 var t=confirm("Are you sure you want to Approve this scrap?");
		 if(t==true)
		 {
			 $('body').Wload({text:' Loading'});
					$.ajax({
					type : 'post',
					url : base_path+'admin/Scrap_controller/approve_bid',
					data : {
						bid_id : bid_id,scrap_id:scrap_id
					},
					success : function(data) {
						  $('body').Wload('hide',{time:100});
					if(data==1){
					
							alert("You approved this Bid");
						 $("#approve_span").hide();
						  $("#sold_out").hide();
						 $("#approved_span").show();
						window.location=base_path+"admin/Scrap_controller/all_bid"
						
					}else if(data==2){
						alert("You already approved this bid");
					}
					else {
						alert("Somthing went wrong....! Please try after some time. Thank you.");
					}
		 }
	});
}
		 
	}

var base_path='<?php echo base_url();?>';
 function markasview(id)
{
	var r = confirm("Are you sure you want to mark this interest as viewed?");
	if(r==true)
	{
	$.ajax({
			type : 'post',
			url : base_path+'admin/Scrap_controller/change_bid_status',
			data : {
				id : id
			},
			success : function(data) {
		//	alert(data);
			   if(data==1)
			   {
					$("#before_status_change"+id).hide();
					$("#after_status_change"+id).show();
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}
</script>
</body>
</html>