<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Scrap Admin | Log in</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
 
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/formValidation.css"/> 
  <style type="text/css">
    .error{
      color:red;
    }
  </style>
  
  
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
   Scrap admin
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!-- <p class="login-box-msg">Sign in to start your session</p> -->
    <form id="frm_register" name="frm_register" method="post">
    
    
        
		<div class="alert alert-error no-border" id="errorMessage" style="display:none;">

			  <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold"</span>Please enter valid credentials
			</div>
     	<div class="alert alert-success no-border" id="successMessage" style="display:none;">

			  <button data-dismiss="alert" class="close" type="button"><span>×</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold">Well done!</span>You've successfully login
			</div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
       
        <!-- /.col -->
        <div class="col-xs-12 pull-right">
          <button type="submit" name="login" id="login" class="btn btn-primary btn-block btn-flat">Login</button>
          <a class="pull-left" href="<?php echo base_url();?>admin/admin_controller/forgot_password">Forgot password</a>
          
        </div>
        <!-- /.col -->
      </div>
    </form>

   
    <!-- /.social-auth-links -->

    <!-- <a href="#">I forgot my password</a><br> -->
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/formValidation.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/framework/bootstrap.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
   $(document).ready(function() {
	var base_path="<?php echo base_url();?>";
$('#frm_register').formValidation({
        message: 'This value is not valid',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        }
        ,
        fields: {
    
          email: {
            validators: {
              notEmpty: {
                message: 'The email address is required and can\'t be empty'
              }
              ,
              emailAddress: {
                message: 'The input is not a valid email address'
              }
            }
          }
          ,
       
          password: {
            message: 'The password is not valid',
            validators: {
              notEmpty: {
                message: 'Password is required and can\'t be empty'
              }
            }
		  }
		}
		}).on('success.form.fv', function(e) {
		var email=$("#email").val();
		var password=$("#password").val();
		$("#successMessage").hide();
		$("#errorMessage").hide();
	 $.ajax({
                type : 'post',
                url : base_path+'admin/Admin_controller/is_login',
                data : {
                    email : email, password : password
                },
                success : function(data) {
					if(data==1)
					{
							$("#errorMessage").show();
					}else{
						$("#successMessage").show();
						window.location=base_path+'admin/Admin_controller';
					}
				}
	 })
  return false;
		})
   });
</script>
</body>
</html>
