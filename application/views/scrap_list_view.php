<?php $this->load->view("common/header");?>
<style>
.img-width {
    width: 140px;
    height: auto;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Scrap List
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">All Scrap</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1 scrap_list">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Image</th>
										<th>Scrap Id</th>
										<th>Scrap Type</th>
										<th>Organization</th>
										<th>Quantity</th>
										<th>Min bid amt</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($scrap))
							{
								$i=1;
								foreach($scrap as $sc)
								{?>
									<tr id="currentdiv<?php echo $sc['id'];?>">
									<td><?php echo $i;?></td>
									<td><img src="<?php if( $sc['image1']!=''){ echo $sc['image1']; }?>" class="img-thumbnail img-width img-responsive" >
									</td>
									<td><?php if( $sc['order_id']!=''){ echo $sc['order_id'];}?></td>
									<td><?php if( $sc['type']!=''){ echo $sc['type']; }?></td>
									<td><?php if( $sc['org_name']!=''){ echo $sc['org_name'];}?></td>
									<td><?php if( $sc['quantity']!=''){ echo $sc['quantity'];}?></td>
									<td id="minBidAmt<?php echo $sc['id'];?>"><?php if( $sc['min_amt']!=''){ echo $sc['min_amt'];}?></td>
									<td>
									<div id="before_status_change<?php echo $sc['id'];?>">
									<?php if( $sc['status']==0){?><span style="background-color:#D74300; cursor: pointer;" class="label label-success" data-toggle="modal" href="#approveModel<?php echo $sc['id'];?>">Approve</span><?php } elseif( $sc['status']==1) {?><span style="background-color:#f39c12; " class="label" >Approved</span><?php }elseif( $sc['status']==2) {?><span style="background-color:#f44c0e; " class="label" >Sold Out</span><?php }?>
									</div>
										<div id="after_status_change<?php echo $sc['id'];?>" style="display:none;">
										<span style="background-color:#f39c12; " class="label" >Approved</span>
										</div>
											<a href="<?php echo base_url();?>admin/Scrap_controller/view_scrap_view/<?php echo urlencode(base64_encode($sc['id']));?>">
											<span style="background-color:#3C8DBC; cursor: pointer;" class="label">View</span></a>
										<?php if( $sc['status']!=2){ ?>
											<a href="<?php echo base_url();?>admin/Scrap_controller/edit_scrap_view/<?php echo urlencode(base64_encode($sc['id']));?>"><span style="background-color:#EA4335; cursor: pointer;" class="label label-success">Edit</span></a>
										
									<input type="hidden" id="imagename<?php echo $sc['id']; ?>" value="<?php if( $sc['image1']!=''){ echo $sc['image1']; }else{ echo "00";
									}?>">
							
									<span style="background-color:#fc0000; cursor: pointer;" class="label " onclick="delete_data(<?php echo $sc['id']; ?>);">Delete</span>
										<?php }?>
									</td>
									</tr>
									<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                
                </div>
                <div class="clearfix"></div> 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <?php if(!empty($scrap))
							{
							
								foreach($scrap as $sc)
								{?>
  <div id="approveModel<?php echo $sc['id'];?>" class="modal fade" role="dialog">
								  <div class="modal-dialog">
  <div class="control-sidebar-bg"></div>
	<div class="modal-content">
									  <div class="modal-header">
										<a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)">Approved Bid</a>
										<button type="button" class="close" data-dismiss="modal">&times;</button>			
									  </div>
									  
									  <div class="modal-body" id="disputebody">
										<div class="tab-content">
										  <div id="login" class="tab-pane fade in active">
											<form name="login-form" class="clearfix">
												
											  <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter minimum amout for bidding</label>
												  </div>
												  </div>
												  <div class="col-md-10">
												  <div class="form-group">
												  <input class="form-control" type="text"  placeholder="Enter minimum bidding amount" id="bid_amt<?php echo $sc['id'];?>" name="bid_amt<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												   <div class="col-md-2">
												  <div class="form-group">
												  <select class="form-control" id="unit<?php echo $sc['id'];?>" name="unit<?php echo $sc['id'];?>">
												  <option value="kg">kg</option>
												  <option value="ql">ql</option>
												  <option value="ton">ton</option>
												  	</select>
												  </div>
												  </div>
												</div>
												 <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter GST</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input class="form-control" t type="text" placeholder="Enter GST %" id="gst<?php echo $sc['id'];?>" name="gst<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
												 <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter Seller amount</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input class="form-control" type="text" placeholder="Enter Seller amount %" id="seller_amt<?php echo $sc['id'];?>" name="seller_amt<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
											  <div class="form-group mt-10"> 
												<button type="button" onclick="approvescrap(<?php echo $sc['id'];?>);" class="btn btn-default btn-flat btn-theme-colored" >Submit</button> 
											   
											  </div>
											</form>
										  </div>
										  
										</div>
									  </div>
									  
									</div>
									</div>
									</div>
									</div>
								<?php 
						
								}
							}?>
<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();
	
});

var base_path='<?php echo base_url();?>';

function approvescrap(scrap_id)
{
	var bid_amt=$("#bid_amt"+scrap_id).val();
	var gst=$("#gst"+scrap_id).val();
	var seller_amt=$("#seller_amt"+scrap_id).val();
	var unit=$("#unit"+scrap_id).val();
	
	if(bid_amt=='' || gst=='')
	{
		if(bid_amt=='')
		{
			alert("please enter minimun bidding amount");
		}
		if(gst=='')
		{
			alert("please enter GST %");
		}
		
	}else{
	 var t=confirm("Are you sure you want to Approve this scrap?");
		 if(t==true)
		 {
					$.ajax({
					type : 'post',
					url : base_path+'admin/Scrap_controller/approve_scrap',
					data : {
						bid_amt : bid_amt,scrap_id:scrap_id,gst:gst,seller_amt:seller_amt,unit:unit
					},
					success : function(data) {
						//alert(data);
					if(data==1){
					
							alert("You approved this scrap");
//$("#minBidAmt"+scrap_id).html(bid_amt);
						   	$("#approveModel"+scrap_id).modal("hide");
							window.location =base_path+'admin/Scrap_controller';
						
					} else {
						alert("Somthing went wrong....! Please try after some time. Thank you.");
					}
		 }
	});
}
		 }
	}


function delete_data(id)
{
	
	var r = confirm("Are you sure you want to delete current record?");
	if(r==true)
	{
		var img=$("#imagename"+id).val();
	$.ajax({
			type : 'post',
			url : base_path+'admin/Scrap_controller/delete_scrap_data',
			data : {
				id : id,img:img
			},
			success : function(data) {
			//alert(data);
			//alert(data);
			   if(data==1)
			   {
				   alert("successfully deleted Record");
			
				   $("#currentdiv"+id).hide();
				   $("#minBidAmt"+id).html();
				   
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}


</script>
</body>
</html>