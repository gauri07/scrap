<?php $this->load->view("common/header");?>
<style>
.img-width {
    width: 140px;
    height: auto;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Sold Scrap List
        <!-- <small>Optional description</small> -->
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Sold Scrap</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
			<div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
  
            <div class="box-body">
              <div class="row">
			      <div class="col-md-12"> 
      <div class="table-responsive">
						<table class="table table-bordered dataTables-example1">
								<thead>
									<tr>
										<th>Sr No</th>
										<th>Scrap Id</th>
										<th>Organization</th>
										<th>Buyer</th>
										<th>Quantity</th>
										<th>Amount</th>
										<th>GST amt</th>
										<th>Total amt</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($scrap))
							{
								$i=1;
								foreach($scrap as $sc)
								{?>
									<tr id="currentdiv<?php echo $sc['id'];?>">
									<td><?php echo $i;?></td>
								
									<td><a href="<?php echo base_url();?>admin/Scrap_controller/view_scrap_view/<?php echo urlencode(base64_encode($sc['id']));?>"><?php if( $sc['order_id']!=''){ echo $sc['order_id'];}?></a></td>
									<td><a href="<?php echo base_url();?>admin/Admin_controller/view_seller_view/<?php echo urlencode(base64_encode($sc['seller_id']));?>"><?php if( $sc['org_name']!=''){ echo $sc['org_name'];}?></a></td>
									<td><a href="<?php echo base_url();?>admin/Admin_controller/view_buyer_view/<?php echo urlencode(base64_encode($sc['buyer_id']));?>"><?php if( $sc['name']!=''){ echo $sc['name'];}?></a></td>
									<td><?php if( $sc['quantity']!=''){ echo $sc['quantity'];}?></td>
									<td><?php if( $sc['total_amt']!=''){ echo round($sc['total_amt'],2);}?></td>
									<td><?php if( $sc['gst']!=''){ echo $sc['gst'];}?></td>
									<td><?php if( $sc['sold_amt']!=''){ echo round($sc['sold_amt'],2);}?></td>
									
								
									</tr>
									<?php 
							$i++;
								}
							}?>
							</tbody>
							</table>
							</div>
							</div>
              </div>
            
           </div>
        </div>
        <!-- /.box-body -->
     </div>
                
                </div>
                <div class="clearfix"></div> 
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  


  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <?php if(!empty($scrap))
							{
							
								foreach($scrap as $sc)
								{?>
  <div id="approveModel<?php echo $sc['id'];?>" class="modal fade" role="dialog">
								  <div class="modal-dialog">
  <div class="control-sidebar-bg"></div>
	<div class="modal-content">
									  <div class="modal-header">
										<a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="<?php echo base_url();?>images/logo-wide.png" alt=""></a>
										<button type="button" class="close" data-dismiss="modal">&times;</button>			
									  </div>
									  
									  <div class="modal-body" id="disputebody">
										<div class="tab-content">
										  <div id="login" class="tab-pane fade in active">
											<form name="login-form" class="clearfix">
												
											  <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter minimum amout for bidding</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input type="text"  id="bid_amt<?php echo $sc['id'];?>" name="bid_amt<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
												 <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter GST</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input type="text" placeholder="Enter GST %" id="gst<?php echo $sc['id'];?>" name="gst<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
												 <div class="row">
												<div class="col-md-12">
												<div class="form-group">
												  <label for="form_username_email">Enter Seller amount</label>
												  </div>
												  </div>
												  <div class="col-md-12">
												  <div class="form-group">
												  <input type="text" placeholder="Enter Seller amount %" id="seller_amt<?php echo $sc['id'];?>" name="seller_amt<?php echo $sc['id'];?>" >
												  </div>
												  </div>
												</div>
											  <div class="form-group mt-10">
											<div class="col-md-4">
												<button type="button" onclick="approvescrap(<?php echo $sc['id'];?>);" class="btn btn-default btn-flat btn-theme-colored" >Submit</button>
											  </div>
											  <div class="clear text-center pt-10">
												
											  </div>
											</form>
										  </div>
										  
										</div>
									  </div>
									  
									</div>
									</div>
									</div>
									</div>
								<?php 
						
								}
							}?>
<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>
<script>
$(document).ready(function(){
	$('.dataTables-example1').dataTable();
	
});

var base_path='<?php echo base_url();?>';

function approvescrap(scrap_id)
{
	var bid_amt=$("#bid_amt"+scrap_id).val();
	var gst=$("#gst"+scrap_id).val();
	var seller_amt=$("#seller_amt"+scrap_id).val();
	
	if(bid_amt=='' || gst=='')
	{
		if(bid_amt=='')
		{
			alert("please enter minimun bidding amount");
		}
		if(gst=='')
		{
			alert("please enter GST %");
		}
		
	}else{
	 var t=confirm("Are you sure you want to Approve this scrap?");
		 if(t==true)
		 {
					$.ajax({
					type : 'post',
					url : base_path+'admin/Scrap_controller/approve_scrap',
					data : {
						bid_amt : bid_amt,scrap_id:scrap_id,gst:gst,seller_amt:seller_amt
					},
					success : function(data) {
						//alert(data);
					if(data==1){
					
							alert("You approved this scrap");
//$("#minBidAmt"+scrap_id).html(bid_amt);
						   	$("#approveModel"+scrap_id).modal("hide");
							window.location =base_path+'admin/Scrap_controller';
						
					} else {
						alert("Somthing went wrong....! Please try after some time. Thank you.");
					}
		 }
	});
}
		 }
	}


function delete_data(id)
{
	
	var r = confirm("Are you sure you want to delete current record?");
	if(r==true)
	{
		var img=$("#imagename"+id).val();
	$.ajax({
			type : 'post',
			url : base_path+'admin/Scrap_controller/delete_scrap_data',
			data : {
				id : id,img:img
			},
			success : function(data) {
			//alert(data);
			//alert(data);
			   if(data==1)
			   {
				   alert("successfully deleted Record");
			
				   $("#currentdiv"+id).hide();
				   $("#minBidAmt"+id).html();
				   
			   }else
			   {
				alert("something went wrong..please try later");
				}
			}
	});
	}
}


</script>
</body>
</html>