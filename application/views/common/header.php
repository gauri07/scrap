<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/logo/email.png"/>
  <title>Ezyscrap</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  
<link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/dist/css/dataTables/dataTables.tableTools.min.css" rel="stylesheet" type="text/css">
	 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/form_validation/formValidation.css"/> 
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/skin-green.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/style.css">
  	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/loader/css/jquery.Wload.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-green  sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url();?>assets/dist/img/logo.png" class="img-logo" alt="Logo"></span>
      <!-- logo for regular state and mobile devices -->
	    <!-- <image rel="icon" type="image/png" src="<?php echo base_url();?>assets/logo/email.png"/> -->
      <span class="logo-lg"><img src="<?php echo base_url();?>assets/dist/img/logo.png" class="img-logo" alt="Logo"> <b>Ezyscrap</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
     <!-- <a href="<?php echo base_url();?>assets/#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>-->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
  
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            
			 <div class="pull-right">
                  <a href="<?php echo base_url();?>admin/Admin_controller/signout/<?php 
			  $admin_info=$this->session->userdata('admin_info');
			  if(!empty($admin_info)){echo urlencode(base64_encode($admin_info['id']));}?>" class="btn btn-default btn-flat btn-sign-out"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a>
                </div>
          
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel">
       <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>-->
        <!-- <div class="pull-left info">
          <p>Admin</p> -->
          <!-- Status -->
        
        <!-- </div>
      </div> -->

    

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
       
        <!-- Optionally, you can add icons to the links -->
       <!--  <li class="header">MAIN NAVIGATION</li>-->
        <li class="active"><a href="<?php echo base_url();?>admin/Admin_controller"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		   <li><a href="<?php echo base_url();?>admin/Scrap_controller/scrap_type_view"><i class="fa fa-cogs"></i> <span>Scrap Type</span></a></li>
			 <li><a href="<?php echo base_url();?>admin/Scrap_controller/all_interests"><i class="fa fa-money"></i> <span>Scrap Interest</span></a></li>
			 <li><a href="<?php echo base_url();?>admin/Scrap_controller/all_bid"><i class="fa fa-gavel"></i> <span>Bid</span></a></li>
		  <li class="treeview">
            <a href="#"><i class="fa fa-cog"></i> <span>Scrap</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>admin/Scrap_controller">All Scrap</a></li>
                <li><a href="<?php echo base_url();?>admin/Scrap_controller/sold_scrap">Sold Scrap</a></li>
                 <li><a href="<?php echo base_url();?>admin/Scrap_controller/add_scrap">Add Scrap</a></li>
            </ul>
          </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-user"></i> <span>Buyer</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>admin/Admin_controller/buyer_list">All Buyer</a></li>
                <li><a href="<?php echo base_url();?>admin/Admin_controller/add_buyer_view">Add Buyer</a></li>
            </ul>
          </li>
		        <li class="treeview">
            <a href="#"><i class="fa fa-user-secret"></i> <span>Seller</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?php echo base_url();?>admin/Admin_controller/seller_list">All Seller</a></li>
                <li><a href="<?php echo base_url();?>admin/Admin_controller/add_seller_view">Add Seller</a></li>
            </ul>
          </li>
		    
		   
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>