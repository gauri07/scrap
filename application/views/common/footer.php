
 <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
  
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="<?php echo base_url();?>admin/Admin_controller">Ezyscrap</a>.</strong> All rights reserved.
  </footer>
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/dist/js/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/dist/js/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/dist/js/dataTables/dataTables.responsive.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/dist/js/dataTables/dataTables.tableTools.min.js"></script>

 <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/formValidation.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/framework/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>assets/loader/js/jquery.Wload.js"></script>

  
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
	 
	 
</body>
</html>