<?php $this->load->view("common/header");?>
<style>
.btn-bs-file{
    position:relative;
}
.btn-bs-file input[type="file"]{
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width:0;
    height:0;
    outline: none;
    cursor: inherit;
}
	.qty-block label {
		/* float: left; */
		width: 100%;
	}
	.qty-block .form-control {
		width: auto;
		margin-right: 5px;
	}
	.qty-block.has-error .help-block {
		color: #dd4b39;
		float: left;
		width: 100%;
	}

</style>
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Scrap info
        <!-- <small>Optional description</small> -->
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12">   
		   <?php if($this->session->flashdata('success')!=''){;?>
			<div class="alert alert-success no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold">Well done!</span>  <?php echo $this->session->flashdata('success');?>
			</div>
			 <?php } ?>
			 <?php if($this->session->flashdata('error')!=''){;?>
			<div class="alert alert-error no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold"</span>  <?php echo $this->session->flashdata('error');?>
			</div>
			 <?php } ?>
			 <div class="row">
			 <div class="col-md-12">
					    <div class="col-md-3" id="imagediv1">
						  <img id="scrapimage1" src="<?php if($scrap['image1']!=''){echo $scrap['image1'];}else{
							  echo base_url().'assets/logo/blank.jpg';  } ?>" class="img-thumbnail img-width" >
						  <?php if($scrap['image1']!=''){?>
						 <div class="bottom-left" ><i class="fa fa-trash-o" aria-hidden="true" onclick="delete_image(<?php echo $scrap['id']; ?>,'1','<?php echo $scrap['image1'];?>','<?php echo urlencode(base64_encode($scrap['id']));?>');"></i>
						 <label class="btn-bs-file btn ">
                	 <i class="fa fa-edit btn-bs-file" aria-hidden="true">	</i>
                <input type="file" id="image1" name="image1" onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage1','image1','1','<?php echo urlencode(base64_encode($scrap['id']));?>')"/>
								</label>
						 </div>
						
						  <?php }else
							{?>
								<label class="  ">
							   <i class="fa fa-add btn-bs-file" aria-hidden="true">	</i>
							<input type="file" id="imagea1" name="imagea1"  onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage1','imagea1','1','<?php echo urlencode(base64_encode($scrap['id']));?>')"/>
							</label>
						 <?php }?>
            
						</div>
						
					     <div class="col-md-3" id="imagediv2">
						  <img id="scrapimage2" src="<?php if($scrap['image2']!=''){echo $scrap['image2'];}else{
							  echo base_url().'assets/logo/blank.jpg';  } ?>" class="img-thumbnail img-width" >
						  <?php if($scrap['image2']!=''){?>
						 <div class="bottom-left" ><i class="fa fa-trash-o"  aria-hidden="true"onclick="delete_image(<?php echo $scrap['id']; ?>,'2','<?php echo $scrap['image2'];?>','<?php echo urlencode(base64_encode($scrap['id']));?>');"></i>
						<label class="btn-bs-file btn ">
                	 <i class="fa fa-edit btn-bs-file" aria-hidden="true">	</i>
								<input type="file" id="image2" name="image2" onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage2','image2','2','<?php echo urlencode(base64_encode($scrap['id']));?>');"/>
								</label>
								</div>	
						  <?php }else
							{?>
								<label class=" ">
							   <i class="fa fa-add btn-bs-file" aria-hidden="true">	</i>
							<input type="file" id="image22" name="image22"  onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage2','image22','2','<?php echo urlencode(base64_encode($scrap['id']));?>');"/>  </label>
						 <?php }?>
          
						</div>
						
					   <div class="col-md-3" id="imagediv3">
						  <img id="scrapimage3" src="<?php if($scrap['image3']!=''){echo $scrap['image3'];}else{
							  echo base_url().'assets/logo/blank.jpg';  } ?>" class="img-thumbnail img-width" >
						  <?php if($scrap['image3']!=''){?>
						 <div class="bottom-left" ><i class="fa fa-trash-o" aria-hidden="true"onclick="delete_image(<?php echo $scrap['id']; ?>,'3','<?php echo $scrap['image3'];?>','<?php echo urlencode(base64_encode($scrap['id']));?>');"></i> 
						<label class="btn-bs-file btn ">
                	 <i class="fa fa-edit btn-bs-file" aria-hidden="true">	</i>
								<input type="file" id="image3" name="image3" onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage3','image3','3','<?php echo urlencode(base64_encode($scrap['id']));?>')"/>
						</label>
						</div>
						
						  <?php }else
							{?>
								<label class="  ">
							   <i class="fa fa-add btn-bs-file" aria-hidden="true">	</i>
							<input type="file" id="image33" name="image33"  onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage3','image33','3','<?php echo urlencode(base64_encode($scrap['id']));?>')"/></label>
						 <?php }?>
            
						</div>
						
						
						
						   <div class="col-md-3" id="imagediv4">
						  <img id="scrapimage4" src="<?php if($scrap['image4']!=''){echo $scrap['image4'];}else{
							  echo base_url().'assets/logo/blank.jpg';  } ?>" class="img-thumbnail img-width" >
						  <?php if($scrap['image4']!=''){?>
						 <div class="bottom-left" ><i class="fa fa-trash-o" aria-hidden="true"onclick="delete_image(<?php echo $scrap['id']; ?>,'4','<?php echo $scrap['image4'];?>','<?php echo urlencode(base64_encode($scrap['id']));?>');"></i>
						<label class="btn-bs-file btn ">
                	 <i class="fa fa-edit btn-bs-file" aria-hidden="true">	</i>
								<input type="file" id="image4" name="image4" onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage4','image4','4','<?php echo urlencode(base64_encode($scrap['id']));?>')"/>
								</label>
								</div>
						  <?php }else
						  {?><label class="  ">
							   <i class="fa fa-add btn-bs-file" aria-hidden="true">	</i>
							<input type="file" id="image44" name="image44"  onchange="changeimage(this,<?php echo $scrap['id'];?>,<?php echo $scrap['seller_id'];?>,'scrapimage4','image44','4','<?php echo urlencode(base64_encode($scrap['id']));?>')"/></label>
						 <?php }?>
            
						</div>
			 </div>
			 </div>
            <!-- form start -->
            <form role="form" name="scrap_edit" id="scrap_edit" method="POST" action="<?php echo base_url();?>admin/Scrap_controller/edit_scrap_info" enctype="multipart/form-data" >  
              <div class="box box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">Scrap Info</h3>
                  </div>
                  <!-- /.box-header --> 
                    <div class="box-body">
					<div class="form-group">
					 <input class="form-control" id="id" name="id" value="<?php if(isset($scrap["id"])){ echo $scrap["id"];}?>" placeholder="Enter Name"  type="hidden">
					
					 </div>
                            <div class="form-group">
                        <label for="exampleInputEmail1">Scrap type</label>
						<select class="form-control" id="type_id" name="type_id">
					
						<?php if(!empty($scrap_type))
						{
							foreach($scrap_type as $st)
							{
							//echo $scrap['type_id'];
							//echo $st['id'];die;
							if(isset($scrap['type_id']) && $st['id']==$scrap['type_id']){
								//echo "hii";die;
								
							}
							?>
								<option  value="<?php echo $st["id"];?>" <?php if(isset($scrap['type_id']) && $st['id']==$scrap['type_id']){?>selected <?php };?>><?php echo $st["type"];?></option>
								
						<?php }
						}?>
								</select>
                      </div> 
					    <div class="form-group">
                        <label for="exampleInputEmail1">Seller</label>
						<select class="form-control" id="seller_id" name="seller_id">
						
						<?php if(!empty($seller))
						{
							foreach($seller as $st)
							{?>
							
								<option value="<?php echo $st["id"];?>"  <?php if(isset($scrap['seller_id']) && $st['id']==$scrap['seller_id']){;?>selected <?php };?>><?php echo $st["org_name"];?></option>
						<?php }
						}?>
								</select>
                      </div> 
                    
                     <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <input class="form-control" id="desc" name="desc" value="<?php if(isset($scrap["desc"])){ echo $scrap["desc"];}?>" placeholder="Enter Mobile number" type="text">
                      </div>
					  <?php if(isset($scrap["quantity"])){
						 $qty=explode(' ',$scrap["quantity"]);
						// print_r($qty);die;
						?>
					  
					    <div class="form-group qty-block">
                        <label for="exampleInputEmail1">Quantity</label>
                        <input class="form-control pull-left" id="quantity" name="quantity" placeholder="Enter Quantity" value="<?php echo $qty[0];?>" type="text">
                     <select class="form-control pull-left" id="unit" name="unit">
						
						<option value="kg" <?php if($qty[1]=='kg'){?> selected <?php }?>>kg</option>
						<option value="ton" <?php if($qty[1]=='ton'){?> selected <?php }?>>ton</option>
						<option value="ql" <?php if($qty[1]=='ql'){?> selected <?php }?>>ql</option>
								</select>
					 </div>
					 <?php }?>
					 <br>
					   <?php if(isset($scrap["min_amt"])){
						 $qty1=explode(' ',$scrap["min_amt"]);
						// print_r($qty);die;
						?>
					  
					    <div class="form-group qty-block">
                        <label for="exampleInputEmail1">Minimum Bid amount </label>
                        <input class="form-control pull-left" id="min_amt" name="min_amt" placeholder="Enter seller amount %"  value="<?php echo $qty1[0];?>" type="text" >
						  <select class="form-control pull-left" id="unit1" name="unit1">
						
						<option value="kg" <?php if($qty1[1]=='kg'){?> selected <?php }?>>kg</option>
						<option value="ton" <?php if($qty1[1]=='ton'){?> selected <?php }?>>ton</option>
						<option value="ql" <?php if($qty1[1]=='ql'){?> selected <?php }?>>ql</option>
						
								</select>
                   </div>
						<?php }?>
						 <br>
					    <div class="form-group">
                        <label for="exampleInputEmail1">GST %</label>
                        <input class="form-control" id="gst" name="gst" placeholder="Enter gst %"  value="<?php if(isset($scrap["gst"])){ echo $scrap["gst"];}?>" type="text">
                       </div>
					    <div class="form-group">
                        <label for="exampleInputEmail1">Seller amount %</label>
                        <input class="form-control" id="seller_amt" name="seller_amt" placeholder="Enter seller amount %"  value="<?php if(isset($scrap["seller_amt_per"])){ echo $scrap["seller_amt_per"];}?>" type="text">
                       </div>
					  
						   
                     
               <div class="form-group">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div> 
				  </div>
                    </div> 
                    </div> 
              
	
 
            </form>
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<?php $this->load->view("common/footer");?>

 <script>
 var ajax_base_path='<?php echo base_url();?>';
  function changeimage(input,id,seller_id,divname,file_name,flag,encoded_id)
{
		var data = new FormData();
	data.append('image', $('#'+file_name+'').prop('files')[0]);
	data.append('id', id);
	data.append('seller_id', seller_id);
	data.append('flag', flag);
			$.ajax({
							type : 'post',
							url : ajax_base_path+'admin/Scrap_controller/save_image',
							data: data,
							processData: false, // important
							contentType: false, // important
							success : function(data) {
								if(data==1)
								{
										alert("image updated successfully");
										 window.location =ajax_base_path+'admin/Scrap_controller/edit_scrap_view/'+encoded_id;
										/*  if (input.files && input.files[0]) {
										var reader = new FileReader();

										reader.onload = function (e) {
											$('#'+divname+'').attr('src', e.target.result);
											   
										};

            reader.readAsDataURL(input.files[0]);
        } */
								}else
								{
									alert("erro occured in uploading image");
								}
							}
			});
			//alert(divname);
			
}

function delete_image(scrap_id,id,image,encoded_id)
{
		var r = confirm("Are you sure you want to delete image.?");
		var name="image"+id;
		if(r==true)
		{
			//alert("hii");
			$.ajax({
				type : 'post',
				url : ajax_base_path+'admin/Scrap_controller/delete_image',
				data : {
					scrap_id:scrap_id,name : name,image : image
				},
				success : function(data) {
				//alert(data);
				   if(data==1)
				   {
					   alert("successfully deleted image");
					   $("#imagediv"+id).hide();
					   window.location =ajax_base_path+'admin/Scrap_controller/edit_scrap_view/'+encoded_id;
				   }
					else
				   {
					alert("something went wrong..please try later");
					}
				}
			});
		}
	
	
}
 </script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/custom_js/add_scrap.js"></script>
</body>
</html>