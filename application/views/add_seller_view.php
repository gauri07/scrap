<?php $this->load->view("common/header");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Add New Seller
        <!-- <small>Optional description</small> -->
      </h1>
   
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        --------------------------> 
       <!--- Main Row Start --> 
        <div class="row">
          <div class="col-md-12">   
		   <?php if($this->session->flashdata('success')!=''){;?>
			<div class="alert alert-success no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold">Well done!</span>  <?php echo $this->session->flashdata('success');?>
			</div>
			 <?php } ?>
			 <?php if($this->session->flashdata('error')!=''){;?>
			<div class="alert alert-error no-border">

			  <button data-dismiss="alert" class="close" type="button"><span>�</span><span class="sr-only">Close</span></button>

			  <span class="text-semibold"</span>  <?php echo $this->session->flashdata('error');?>
			</div>
			 <?php } ?>
            <!-- form start -->
            <form role="form" name="user_detail" id="user_detail" method="POST" action="<?php echo base_url();?>admin/Admin_controller/add_seller_info" enctype="multipart/form-data" >  
              <div class="box box-primary ">
                  <div class="box-header with-border">
                    <h3 class="box-title">Buyer Info</h3>
                  </div>
                  <!-- /.box-header --> 
                    <div class="box-body">
					
                      <div class="form-group">
                        <label for="exampleInputEmail1">Organization Name</label>
                        <input class="form-control" id="org_name" name="org_name" placeholder="Enter Organization Name"  type="text">
                      </div> 
					  <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input class="form-control" id="email" name="email" placeholder="Enter email" type="text">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input class="form-control" id="password" name="password" placeholder="Enter Password" type="password">
                      </div>
					  <div class="form-group">
                        <label for="exampleInputPassword1">Confirm Password</label>
                        <input class="form-control" id="conf_pwd" name="conf_pwd" placeholder="Enter Confirm Password" type="password">
                      </div><div class="form-group">
                        <label for="exampleInputPassword1">Mobile number</label>
                        <input class="form-control" id="contact_no" name="contact_no" placeholder="Enter Mobile number" type="text">
                      </div>
					  
					   <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input class="form-control" id="address" name="address" placeholder="Address" type="text">
                      </div> 
					  <div class="form-group">
                        <label for="exampleInputEmail1">City</label>
                        <input class="form-control" id="city" name="city" placeholder="city" type="text">
                      </div>   <div class="form-group">
                        <label for="exampleInputEmail1">Zip</label>
                        <input class="form-control" id="zip" name="zip" placeholder="zip" type="text">
                      </div> 
                    
              
                    </div> 
                </div>
	
  <div class="form-group">
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div> 
				  </div>
            </form>
          </div> 
        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<?php $this->load->view("common/footer");?>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/custom_js/add_seller.js"></script>
</body>
</html>