<?php $this->load->view("common/header");?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <!-- <small>Optional description</small> -->
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

        <div class="row home-tab">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text"> Buyer</span>
                <span class="info-box-number"><?php if(!empty($buyer)){echo $buyer['cnt']; }?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-user-secret"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text"> Seller</span>
                <span class="info-box-number"><?php if(!empty($seller)){echo $seller['cnt']; }?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
  
          <!-- fix for small devices only -->
          <div class="clearfix visible-sm-block"></div>
  
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-cogs"></i></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Total Scrap Orders</span>
                <span class="info-box-number"><?php if(!empty($scrap)){echo $scrap['cnt']; }?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-inr" aria-hidden="true"></i> </span>
  
              <div class="info-box-content">
                <span class="info-box-text">Wallet Balance</span>
                <span class="info-box-number"><?php if( $wallet['balance']!=''){ echo round($wallet['balance'],2);}?> </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
    <!-- <div class="row">
          <div class="col-md-12 col-sm-6 col-xs-12">
		   <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">  Admin Wallet Balance</h3>
					   <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
				<div class="box-body"> 
				  <p class="wallet-bal"> <i class="fa fa-inr" aria-hidden="true"></i> <?php //if( $wallet['balance']!=''){ echo $wallet['balance'];}?> </p>
				</div>
				</div>
			</div>
		  </div> -->
	
       <!--- Main Row Start --> 
        <div class="row latest-scrap-block">
          <div class="col-md-8"> 
              <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Latest Scrap</h3>
      
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                         	<th>Image</th>
										<th>Scrap Id</th>
										<th>Scrap Type</th>
										<th>Organization</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php if(!empty($scrap_data))
							{
								$i=1;
								foreach($scrap_data as $sc)
								{?>
							<tr>
							<td><img src="<?php if( $sc['image1']!=''){ echo $sc['image1']; }?>" class="img-thumbnail img-width img-responsive" style="height:4%;">
									</td>
									<td><?php if( $sc['order_id']!=''){ echo $sc['order_id'];}?></td>
									<td><?php if( $sc['type']!=''){ echo $sc['type']; }?></td>
									<td><?php if( $sc['org_name']!=''){ echo $sc['org_name'];}?></td>
							</tr>
							<?php 
								}
							}?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.table-responsive -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer clearfix">
                    <a href="<?php echo base_url();?>admin/Scrap_controller" class="btn btn-sm btn-default btn-flat pull-right">View All Scrap</a>
                  </div>
                  <!-- /.box-footer -->
                </div>
                <div class="clearfix"></div> 
          </div>
		  <!-- seller-->
  <div class="col-md-4">
               <!-- USERS LIST -->
               <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">Latest Seller</h3>
  
                    <div class="box-tools pull-right">
                      <span class="label label-danger">4 New Seller</span>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <ul class="users-list clearfix">
					              <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                         	<th>Organization Name</th>
										<th>phone number</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php if(!empty($seller_data))
							{
								$i=1;
								foreach($seller_data as $sc)
								{?>
							<tr>
									<td><?php if( $sc['org_name']!=''){ echo $sc['org_name']; }?></td>
									<td><?php if( $sc['phone_no']!=''){ echo $sc['phone_no'];}?></td>
							</tr>
							<?php 
								}
							}?>
                        </tbody>
                      </table>
                    </div>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                    <a href="<?php echo base_url();?>admin/Admin_controller/seller_list" class="uppercase">View All Seller</a>
                  </div>
                  <!-- /.box-footer -->
                </div>
                <!--/.box --> 
          </div>
		   <!-- buyer-->
          <div class="col-md-4">
               <!-- USERS LIST -->
               <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title">Latest Buyer</h3>
  
                    <div class="box-tools pull-right">
                      <span class="label label-danger">4 New Buyer</span>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <ul class="users-list clearfix">
					              <div class="table-responsive">
                      <table class="table no-margin">
                        <thead>
                        <tr>
                         	<th>Name</th>
										<th>phone number</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php if(!empty($buyer_data))
							{
								$i=1;
								foreach($buyer_data as $sc)
								{?>
							<tr>
									<td><?php if( $sc['name']!=''){ echo $sc['name']; }?></td>
									<td><?php if( $sc['phone_no']!=''){ echo $sc['phone_no'];}?></td>
							</tr>
							<?php 
								}
							}?>
                        </tbody>
                      </table>
                    </div>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                    <a href="<?php echo base_url();?>admin/Admin_controller/buyer_list" class="uppercase">View All Buyer</a>
                  </div>
                  <!-- /.box-footer -->
                </div>
                <!--/.box --> 
          </div>

        </div>

       <!--- Main Row End -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view("common/footer");?>