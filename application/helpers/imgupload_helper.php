<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('uploadImg'))
{
	function uploadImg($file,$width,$height,$id,$path)
	{
		if(isset($file['image']['name']) && $file['image']['size'] != 0)
		{
			$ci =& get_instance();
			$config['upload_path'] = $path;
			$image_name=$id."img".strtolower($file['image']['name']);
			$config['tmp_name'] =$image_name;
			$config['allowed_types'] ='gif|jpg|png|img|jpeg';
			$ci->upload->initialize($config);
			if (!$ci->upload->do_upload('image'))
			{
				//echo $ci->upload->display_errors();die;
				$ci->form_validation->set_message('fileUploadErrorMsg', $ci->upload->display_errors());
				$ci->session->set_flashdata('fileUploadErrorMsg', $ci->upload->display_errors());
				return FALSE;
			}
			else
			{
				//echo "In";die;
				$img =  $ci->upload->data();
				/* $config['image_library'] = 'gd2';
				$config['source_image'] = './uploads/'.$id.'/'.$path.'/'.$img['file_name'];
				$config['new_image'] = './uploads/'.$id.'/'.$path.'/thumbs/'.$img['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = FALSE;
				$config['width'] = $width;
				$config['height'] = $height;
				$ci->image_lib->initialize($config); 
				$return = $ci->image_lib->resize();*/
			//	generate_large_thumbs($id,$path,$img['file_name']);
				//generate_small_thumbs($id,$path,$img['file_name']);
				return $img['file_name'];
				
			}
		}
	}
	
	function generate_large_thumbs($id,$path,$img)
	{
		$ci =& get_instance();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/'.$id.'/'.$path.'/'.$img;
		$config['new_image'] = './uploads/'.$id.'/'.$path.'/largethumbs/'.$img;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = 260;
		$config['height'] = 217;
		$ci->image_lib->initialize($config);
		$return = $ci->image_lib->resize();
		return TRUE;
	}
	function generate_small_thumbs($id,$path,$img)
	{
		$ci =& get_instance();
		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/'.$id.'/'.$path.'/'.$img;
		$config['new_image'] = './uploads/'.$id.'/'.$path.'/smallthumbs/'.$img;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = 44;
		$config['height'] = 45;
		$ci->image_lib->initialize($config);
		$return = $ci->image_lib->resize();
		return TRUE;
	}
	
	
	
}
	 function sendNotification($dataArr) {
		 
		  //fcm device ids array which is created previously
			
    	$fcmApiKey = 'AAAA_lQ03qI:APA91bHP0-jBpgelE_v8rdatyHw6oEhzxcWqsYbyR4jGW-8zazbVfCx65YFgScRIHur3sc7GprIghoANMgRALl-OQYkmbIBtv8CPCjOI7keDSQ9ObGHRiiHKJzpXIfNO3GBjzzYKDWCW';
		//App API Key(This is google cloud messaging api key not web api key)
        $url = 'https://fcm.googleapis.com/fcm/send';//Google URL

    	$registrationIds = $dataArr['device_id'];//Fcm Device ids array

    	$message = $dataArr['message'];//Message which you want to send
        $title =$dataArr['title'];

        // prepare the bundle
        $msg = array('message' => $message,'title' => $title);
        $fields = array('registration_ids' => $registrationIds,'data' => $msg);
		
//print_r( $fields);die;
        $headers = array(
            'Authorization: key=' . $fcmApiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $url );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        // Execute post
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);    
 
     return $result;
    }