$(document).ready(function() {

$('#user_detail').formValidation({

message: 'This value is not valid',

icon: {

valid: 'glyphicon glyphicon-ok',

invalid: 'glyphicon glyphicon-remove',

validating: 'glyphicon glyphicon-refresh'

},

fields: {

      
		org_name: {

            validators: {

            notEmpty: {

            message: 'Please Enter Name'

            },

            regexp: {

            regexp: /^[a-zA-Z\s]+$/i,

            message:' Name can only consist of alphabetical characters'

            }

            }

        },
		email: {

			validators: {

			notEmpty: {

			message: 'Email id is required and cannot be empty'

			},

			emailAddress: {

			regexp:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

			message: 'This input is not a valid email address'

			}

			}

		},

        contact_no: {

              verbos:'false',

              trigger: 'blur',

                validators: {

                    notEmpty: {

                        message: 'Please Enter Contact Number'

                    },

                    stringLength: {

                        min: 10,

                        max: 10,

                        message: 'The Contact Number must be 10 digit long'

                    },

                    regexp: {

                        regexp: /^[0-9]+$/,

                        message: 'The username can only consist of numbers'

                    }

                }

        },
		password: {

            validators: {

            notEmpty: {

            message: 'Please enter password'

            }
            }

         },
		 conf_pwd: {

            validators: {

            notEmpty: {

            message: 'Please Re enter password'

            },
			identical: {

				field: 'password',

				message: 'Password and confirm password should be same'

				}
            }

         },
	
        
address: {

            validators: {

            notEmpty: {

            message: 'Please Enter address'

            }

            }

         },
		 zip: {

            validators: {

            notEmpty: {

            message: 'Please Enter zip code'

            }

            }

         },
		 city: {

            validators: {

            notEmpty: {

            message: 'Please Enter city'

            }

            }

         }

}

});

$('#edit_seller').formValidation({

message: 'This value is not valid',

icon: {

valid: 'glyphicon glyphicon-ok',

invalid: 'glyphicon glyphicon-remove',

validating: 'glyphicon glyphicon-refresh'

},

fields: {

      
		org_name: {

            validators: {

            notEmpty: {

            message: 'Please Enter Name'

            },

            regexp: {

            regexp: /^[a-zA-Z\s]+$/i,

            message:' Name can only consist of alphabetical characters'

            }

            }

        },
		email: {

			validators: {

			notEmpty: {

			message: 'Email id is required and cannot be empty'

			},

			emailAddress: {

			regexp:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

			message: 'This input is not a valid email address'

			}

			}

		},

        contact_no: {

              verbos:'false',

              trigger: 'blur',

                validators: {

                    notEmpty: {

                        message: 'Please Enter Contact Number'

                    },

                    stringLength: {

                        min: 10,

                        max: 10,

                        message: 'The Contact Number must be 10 digit long'

                    },

                    regexp: {

                        regexp: /^[0-9]+$/,

                        message: 'The username can only consist of numbers'

                    }

                }

        },
		
        
address: {

            validators: {

            notEmpty: {

            message: 'Please Enter address'

            }

            }

         },
		 zip: {

            validators: {

            notEmpty: {

            message: 'Please Enter zip code'

            }

            }

         },
		 city: {

            validators: {

            notEmpty: {

            message: 'Please Enter city'

            }

            }

         }

}

});
});