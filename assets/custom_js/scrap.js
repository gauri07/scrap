$(document).ready(function() {

$('#scrap_add').formValidation({

message: 'This value is not valid',

icon: {

valid: 'glyphicon glyphicon-ok',

invalid: 'glyphicon glyphicon-remove',

validating: 'glyphicon glyphicon-refresh'

},

fields: {

      
		scrap_type: {

            validators: {

            notEmpty: {

            message: 'Please select scrap type'

            }

          
            }

        },
		seller_id: {

			validators: {

			notEmpty: {

			message: 'Please select Seller '

			}


			}

		},
		image: {

			validators: {

			notEmpty: {

			message: 'Please select at least one image '

			}


			}

		},unit: {

			validators: {

			notEmpty: {

			message: 'Please select unit'

			}


			}

		},

        qty: {

              verbos:'false',

              trigger: 'blur',

                validators: {

                    notEmpty: {

                        message: 'Please Enter Quantity'

                    }

                }

        },
		decs: {

            validators: {

            notEmpty: {

            message: 'Please enter scrap description'

            }
            }

         }

}

}).on('success.form.fv', function(e) {
	 var $fileUpload = $("input[type='file']");
        if (parseInt($fileUpload.get(0).files.length)>4){
         alert("You can only upload a maximum of 4 files");
		   e.preventDefault();
        }
		 if (parseInt($fileUpload.get(0).files.length)==0){
         alert("Please select at least one Image");
		   e.preventDefault();
        }
		
});
});